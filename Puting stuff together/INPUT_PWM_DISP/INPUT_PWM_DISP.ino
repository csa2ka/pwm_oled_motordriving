#include <TaskScheduler.h>
#include <Arduino.h>
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

void refresh_display_task_Callback();
void read_input_task_Callback();

const int FPS = 30;
#define BUTTON1_PIN PA7
#define BUTTON2_PIN PB7
#define BUTTON3_PIN PB8
#define BUTTON4_PIN PB9

char buttonstate1[] = "UP";
char buttonstate2[] = "UP";
char buttonstate3[] = "UP";
char buttonstate4[] = "UP";

Task refresh_display_task(1000/FPS, TASK_FOREVER, &refresh_display_task_Callback);
Task read_input_task(1000/1000, TASK_FOREVER, &read_input_task_Callback);
Scheduler runner;

void refresh_display_task_Callback() {
  show_display();
}

const long SHORT_PRESS_TRESHOLD = 500;
const long LONG_PRESS_TRESHOLD = 2000;

const int ON = 1;
const int OFF = 2;
const int SHORT = 3;
const int LONG = 4;

long button_1_press_start = 0;
int button_1_state = OFF;
long button_2_press_start = 0;
int button_2_state = OFF;
long button_3_press_start = 0;
int button_3_state = OFF;
long button_4_press_start = 0;
int button_4_state = OFF;
void read_input_task_Callback(){
  read_button(BUTTON1_PIN, button_1_press_start, button_1_state);
  read_button(BUTTON2_PIN, button_2_press_start, button_2_state);
  read_button(BUTTON3_PIN, button_3_press_start, button_3_state);
  read_button(BUTTON4_PIN, button_4_press_start, button_4_state);
}

int read_button(int BUTTON_PIN, long &button_press_start, int &button_state){
  long current_millis = millis();
  if (digitalRead(BUTTON_PIN) == HIGH) {
    button_state = OFF;
  } else {
    if (button_state == OFF) {
      button_press_start = current_millis;
      button_state = ON;
    } else {
      if (current_millis - button_press_start > LONG_PRESS_TRESHOLD){
        button_state = LONG;
      } else if (current_millis - button_press_start > SHORT_PRESS_TRESHOLD){
        button_state = SHORT;
      }
    }
  }
}

void updatebutton_text(char *buttonstate,int button_state){
  switch(button_state) {
    case OFF :
      strcpy(buttonstate, "I");
      break;
    case ON :
      strcpy(buttonstate, "P");
      break;
    case LONG :
      strcpy(buttonstate, "L");
      break;
    case SHORT :
      strcpy(buttonstate, "S");
      break;
  }
}


long test_t;
void show_display(){
  updatebutton_text(buttonstate1, button_1_state);
  updatebutton_text(buttonstate2, button_2_state);
  updatebutton_text(buttonstate3, button_3_state);
  updatebutton_text(buttonstate4, button_4_state);
  u8g2.clearBuffer();					// clear the internal memory
  u8g2.setFont(u8g2_font_ncenB08_tr);	// choose a suitable font
  u8g2.drawStr(0,10,u8x8_utoa(test_t));	// write something to the internal memory
  u8g2.drawStr(0,30,buttonstate1);	// write something to the internal memory
  u8g2.drawStr(32,40,buttonstate2);
  u8g2.drawStr(64,30,buttonstate3);
  u8g2.drawStr(80,40,buttonstate4);
  u8g2.sendBuffer();					// transfer internal memory to the display
}

long test_start_t;
void setup () {
  //pinMode(LED_BUILTIN, OUTPUT);
  u8g2.begin();
  runner.init();
  runner.addTask(refresh_display_task);
  runner.addTask(read_input_task);
  delay(1000);
  refresh_display_task.enable();
  read_input_task.enable();

  test_start_t = millis();

  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  pinMode(BUTTON3_PIN, INPUT_PULLUP);
  pinMode(BUTTON4_PIN, INPUT_PULLUP);
}

void loop () {
  runner.execute();
  test_t = millis() - test_start_t;  
  delay(10);
}