#include <math.h>
#include <Arduino.h>
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

double target_freq; //what is the frequency we want to send the PWM to the motor
bool PWM_by_HardverTimer; //Can we use the HardverTimer, or we need to use an other method
bool warning_bool = false;

#define BUTTON1_PIN PB5
#define BUTTON2_PIN PB9
#define BUTTON3_PIN PB7
#define BUTTON4_PIN PB8
#define PWM_PIN PA6
#define DIRECTION_PIN PB1
#define ENCODER_B_PIN PB13
#define ENCODER_A_PIN PB14
#define ENCODER_BUTTON_PIN PB12

 int encoder_counter = 0;
 int aState;
 int aLastState;  
 bool data_changed = false;
 bool encoder_showing_disp = true;
 long encoder_prev_t = 0;
 long last_time_display_was_shown = 0;

HardwareTimer pwmtimer(3);

const int RPM_SCREEN = 1;
const int MICROSTEP_SETTING_SCREEN = 2;
const int MAX_SETTING_SCREEN = 3;
const int MIN_SETTING_SCREEN = 4;
const int SETTING_SCREEN = 5;
int screen = RPM_SCREEN;
//int number = 1;
int start_x = 16;
int start_y = 62;
int delta = 32;
int RPM_TEXT_X = 77;//82;

int microstep = 128; //Should be between these: 1, 2, 4, 8, ... , 128

const long CPU_FREQ = 72000000; //Hertz 
const int STEP_PER_REVOLUTION = 200;
int max_rpm = 300; 
int min_rpm = 20;
int target_rpm = 120;
int current_rpm = 0;


const int STOPPED = 0;
const int STARTING = 1;
const int RUNNING = 2;
const int STOPPING = 3;
const int TURNING_STOPPING = 4;
const int TURNING_STARTING = 5;
int motor_state = STOPPING; 
bool is_there_stop = true;
bool is_there_start = true;
//bool starting = false;
//bool stopping = false;
bool is_PWM_on = false;
bool direction_bool = false;

const int NOTHING_PRESSED = 0;
const int PRESSED_AND_RELEASED = 1;
const int HOLDING_STARTS = 2;
const int HELD = 3;
const int BUTTON_HOLD_DURATION_MINIMUM = 1000;

int button_hold_counts_1 = 0;
int button_state_1 = NOTHING_PRESSED;
bool ignore_next_button_release_1 = true;
int button_hold_counts_2 = 0;
int button_state_2 = NOTHING_PRESSED;
bool ignore_next_button_release_2 = true;
int button_hold_counts_3 = 0;
int button_state_3 = NOTHING_PRESSED;
bool ignore_next_button_release_3 = true;
int button_hold_counts_4 = 0;
int button_state_4 = NOTHING_PRESSED;
bool ignore_next_button_release_4 = true;
int button_hold_counts_encoder = 0;
int button_state_encode = NOTHING_PRESSED;
bool ignore_next_button_release_encode = true;

bool disp_needed = true;

int setting_state = 0;

/*long handle_motor_t = 0;
const int HANDLE_MOTOR_DELTA = 200;
long last_change_time = 0;
long motor_speed_time = 100;
bool motor_handling_needed = false;*/
void handle_motor(){ //smoothen this up
  /*//if (millis() - handle_motor_t > HANDLE_MOTOR_DELTA){
   // handle_motor_t = millis();
  if (current_rpm != target_rpm){
    switch (motor_state){
    case (STARTING):
      if (current_rpm == 0) current_rpm = min_rpm;
      if (current_rpm < target_rpm) {
        if (millis() - last_change_time > motor_speed_time){
          current_rpm = current_rpm += 1;
          motor_handling_needed = true;
          last_change_time = millis();
        }
        //disp_needed = true;
      }
      if (current_rpm > target_rpm) {
        current_rpm = target_rpm;
        motor_handling_needed = false;
        //disp_needed = true;
      }
    break;
    case (STOPPING):
      if (current_rpm > min_rpm) { //decrease current_rpm until min_rpm reached
        //if (tarcurrent_rpm = current_rpm -= 1;
        if (millis() - last_change_time > motor_speed_time){
          current_rpm = current_rpm -= 1;
          last_change_time = millis();
        }
        //disp_needed = true;
      } else if (current_rpm > 0) {
        current_rpm = 0;
        //disp_needed = true;
        pinMode(PWM_PIN, OUTPUT);  //should turn off onl if needed not always
        is_PWM_on = false;
      }
      //if (current_rpm <= min_rpm) current_rpm = 0;
    break;
    case (TURNING_STOPPING):
      if (current_rpm > min_rpm) { //decrease current_rpm until min_rpm reached
        current_rpm = current_rpm * 10/13;
        //disp_needed = true;
      } else {
        current_rpm = min_rpm;
        direction_bool = !direction_bool;
        if (direction_bool) digitalWrite(DIRECTION_PIN, HIGH);
        else digitalWrite(DIRECTION_PIN, LOW);
        motor_state = STARTING;
      }
    break;
    }
  }
  set_rpm(current_rpm);*/ 
}

bool handle_it = true;
void handle_motor_better() {
  bool done = false;
  while (!done){
    switch (motor_state){
      case (STARTING):
        if (current_rpm == 0) current_rpm = min_rpm;
        if (current_rpm < target_rpm) {
          current_rpm += 1;  //current_rpm +1;// * 13 / 10;
        }
        else if (current_rpm > target_rpm){
          current_rpm -= 1;
        } else {
          current_rpm = target_rpm;
          done = true;
        } 
      break;
      case (STOPPING):
        if (current_rpm > min_rpm) { //decrease current_rpm until min_rpm reached
          current_rpm = current_rpm - 1;//* 10 / 13;
        } else {
          current_rpm = 0;
          pinMode(PWM_PIN, OUTPUT);  //should turn off onl if needed not always
          is_PWM_on = false;
          done = true;
        }
        break;
      case (TURNING_STOPPING):
        if (current_rpm > min_rpm) { //decrease current_rpm until min_rpm reached
          current_rpm = current_rpm - 1;//* 10/13;
        } else {
          current_rpm = min_rpm;
          direction_bool = !direction_bool;
          if (direction_bool) digitalWrite(DIRECTION_PIN, HIGH);
          else digitalWrite(DIRECTION_PIN, LOW);
          motor_state = STARTING;
        }
      break;
    }
    set_rpm(current_rpm);
    delay(10);
  }
}

char * TEXT_RPM = "RPM";
char * TEXT_MICROSTEP = "MICROSTEP";
char * TEXT_MAX_RPM = "MAX RPM";
char * TEXT_MIN_RPM = "MIN RPM";

void draw_number_screen(char * heading, int addition, int number, bool is_main_screen){
  u8g2.setFont(u8g2_font_7Segments_26x42_mn);
  if (number > 99){
    u8g2.drawStr(start_x, start_y, u8x8_utoa(number));
  } else if (number > 9){
    u8g2.drawStr(start_x + delta, start_y, u8x8_utoa(number));
  } else {
    u8g2.drawStr(start_x + delta + delta, start_y, u8x8_utoa(number));  
  }
  u8g2.setFont(u8g2_font_logisoso16_tf);//u8g2_font_freedoomr10_mu  );
  if (is_main_screen){
    u8g2.drawStr(start_x , 16, u8x8_utoa(current_rpm));
    if (direction_bool) u8g2.drawTriangle(1,38,9,38,5,30);
    else u8g2.drawTriangle(1,42,9,42,5,50);
    if (warning_bool) u8g2.drawStr(0, 16, "W"); 
  } 	// write something to the internal memory
  u8g2.drawStr(start_x + addition, 16, heading);	// write something to the internal memory
    /*
    u8g2_font_logisoso16_tf
    u8g2_font_freedoomr10_mu
  */
}


char * decide_setting_text (int i) {
  switch (i % 3){
  case 0:
    return TEXT_MAX_RPM;
    break;
  case 1:
    return TEXT_MIN_RPM;
    break;
  case 2:
    return TEXT_MICROSTEP;
    break;
  }
}


void draw_settings_screen(){
    u8g2.setFont(u8g2_font_logisoso16_tf);
    u8g2.drawStr(15, 16, decide_setting_text(setting_state));
    //u8g2.drawStr(10, 20*2, decide_setting_text(setting_state + 1));
    //u8g2.drawStr(10, 20*3, decide_setting_text(setting_state + 2));
    //u8g2.drawStr(10, 16*4, decide_setting_text(setting_state + 3));
}

void show_display(){
  if (disp_needed || encoder_showing_disp /*|| (millis() - last_time_display_was_shown > 1000 && data_changed)*/){
    //handle_motor();//done 
    disp_needed = false;
    u8g2.clearBuffer();					// clear the internal memory
    switch (screen){
    case RPM_SCREEN:
      draw_number_screen("RPM", RPM_TEXT_X-start_x, target_rpm, true);
      u8g2.sendBuffer();
      handle_motor_better();
      u8g2.clearBuffer();		
      draw_number_screen("RPM", RPM_TEXT_X-start_x, target_rpm, true);
      u8g2.sendBuffer();
      break;
    case MICROSTEP_SETTING_SCREEN:
      draw_number_screen(TEXT_MICROSTEP, 0, microstep, false);
      u8g2.sendBuffer();
      break;
    case MAX_SETTING_SCREEN:
      draw_number_screen(TEXT_MAX_RPM, 0, max_rpm, false);
      u8g2.sendBuffer();
      break;
    case MIN_SETTING_SCREEN:
      draw_number_screen(TEXT_MIN_RPM, 0, min_rpm, false);
      u8g2.sendBuffer();
      break;
    case SETTING_SCREEN:
      draw_settings_screen();
      u8g2.sendBuffer();
      break;
    }
    aLastState = digitalRead(ENCODER_A_PIN);
    encoder_showing_disp = false;
    data_changed = false;
  }
}

void update_state(){
  switch(screen) {
    case RPM_SCREEN:
      if (button_state_2 == HOLDING_STARTS) {
        if (is_PWM_on) {
          motor_state = TURNING_STOPPING;  
        } else {
          direction_bool = !direction_bool;
          if (direction_bool) digitalWrite(DIRECTION_PIN, HIGH);
          else digitalWrite(DIRECTION_PIN, LOW);
        }
      }
      if (button_state_2 == PRESSED_AND_RELEASED) {
        if (!is_PWM_on) PWM_turn_on();
        else PWM_turn_off();
      }
      /*if (button_state_3 == PRESSED_AND_RELEASED) target_rpm=target_rpm+5; if (target_rpm>max_rpm) target_rpm=max_rpm;
      if (button_state_4 == PRESSED_AND_RELEASED) target_rpm=target_rpm-5; if (target_rpm<min_rpm) target_rpm=min_rpm;
      //if (button_2_state == SHORT_ENDED) if (is_PWM_on) PWM_turn_off; else PWM_turn_on;
      if (button_state_3 == HOLDING_STARTS) screen = MIN_SETTING_SCREEN;
      if (button_state_4 == HOLDING_STARTS) screen = MICROSTEP_SETTING_SCREEN;*/
      /*if (button_state_1 == HOLDING_STARTS) {
        screen = SETTING_SCREEN;
        disp_needed = true;
      }*/
      if (button_state_2 == PRESSED_AND_RELEASED || button_state_2 == HOLDING_STARTS) 
          disp_needed = true;
      if (button_state_encode == HOLDING_STARTS) {
        screen = SETTING_SCREEN;
        disp_needed = true;
      }
      break;
    case MICROSTEP_SETTING_SCREEN:
      PWM_turn_off();
      /*if (button_state_3 == PRESSED_AND_RELEASED) microstep*=2; if (microstep>128) microstep=128;
      if (button_state_4 == PRESSED_AND_RELEASED) microstep/=2; if (microstep<16) microstep=16;
      if (button_state_3 == HOLDING_STARTS) screen = RPM_SCREEN;
      if (button_state_4 == HOLDING_STARTS) screen = MAX_SETTING_SCREEN;*/
      if (button_state_encode == HOLDING_STARTS) {
        screen = RPM_SCREEN;
        disp_needed = true;
      } 
      if (button_state_encode == PRESSED_AND_RELEASED){
        screen = SETTING_SCREEN;
        disp_needed= true;
      }
      break;
    case MAX_SETTING_SCREEN:
      PWM_turn_off();
      /*if (button_state_3 == PRESSED_AND_RELEASED) max_rpm+=5; if (max_rpm>500) max_rpm=350;
      if (button_state_4 == PRESSED_AND_RELEASED) max_rpm-=5; if (max_rpm<200) max_rpm=200;
      if (button_state_3 == HOLDING_STARTS) screen = MICROSTEP_SETTING_SCREEN;
      if (button_state_4 == HOLDING_STARTS) screen = MIN_SETTING_SCREEN;*/
      if (button_state_encode == HOLDING_STARTS) {
        screen = RPM_SCREEN;
        disp_needed = true;
      } 
      if (button_state_encode == PRESSED_AND_RELEASED){
        screen = SETTING_SCREEN;
        disp_needed= true;
      }
      break;
    case MIN_SETTING_SCREEN:
      PWM_turn_off();
      /*if (button_state_3 == PRESSED_AND_RELEASED) min_rpm+=5; if (min_rpm>200) min_rpm=200;
      if (button_state_4 == PRESSED_AND_RELEASED) min_rpm-=5; if (min_rpm<0) min_rpm=0;
      if (button_state_3 == HOLDING_STARTS) screen = MAX_SETTING_SCREEN;
      if (button_state_4 == HOLDING_STARTS) screen = RPM_SCREEN;*/
      if (button_state_encode == HOLDING_STARTS) {
        screen = RPM_SCREEN;
        disp_needed = true;
      } 
      if (button_state_encode == PRESSED_AND_RELEASED){
        screen = SETTING_SCREEN;
        disp_needed= true;
      }
      break;
    case SETTING_SCREEN:
      PWM_turn_off();
      //if (button_state_3 == PRESSED_AND_RELEASED) setting_state+=1; if (setting_state==5) setting_state=1;
      //if (button_state_4 == PRESSED_AND_RELEASED) setting_state-=1; if (setting_state==0) setting_state=4;
      if (button_state_encode == PRESSED_AND_RELEASED) {
        disp_needed = true;
        switch (setting_state % 3){
          case 0:
            screen = MAX_SETTING_SCREEN;
            break;
          case 1:
            screen = MIN_SETTING_SCREEN;
            break;
          case 2:
            screen = MICROSTEP_SETTING_SCREEN;
            break;
        }
      }
      if (button_state_encode == HOLDING_STARTS){
        screen = RPM_SCREEN;
        disp_needed = true;
      }
      break;
  }
  if (button_state_3 == PRESSED_AND_RELEASED || button_state_3 == HOLDING_STARTS ||
    button_state_4 == PRESSED_AND_RELEASED || button_state_4 == HOLDING_STARTS) 
    disp_needed = true;
}

void setup() {
  u8g2.begin();
  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  pinMode(BUTTON3_PIN, INPUT_PULLUP);
  pinMode(BUTTON4_PIN, INPUT_PULLUP);
  pinMode(DIRECTION_PIN, OUTPUT);
  pinMode(ENCODER_A_PIN, INPUT_PULLUP);
  pinMode(ENCODER_B_PIN, INPUT_PULLUP);
  pinMode(ENCODER_BUTTON_PIN, INPUT_PULLUP);
  pinMode(PWM_PIN, OUTPUT);
  aLastState = digitalRead(ENCODER_A_PIN);   
}


//int button_m = 0;
long buttonState_time_1 = 0;
long buttonState_time_2 = 0;
long buttonState_time_3 = 0;
long buttonState_time_4 = 0;
long buttonState_time_encode = 0;
void updateButtonSate(int PIN, int &button_hold_counts, int &button_state, 
                      bool &ignore_next_button_release, long &buttonState_time){
  if (buttonState_time != millis()){
    bool button_released = false;
    if (digitalRead(PIN) == LOW) button_hold_counts += 1;
    else if (button_hold_counts > 0) {
      button_released = true;
      //button_m = button_hold_counts_1;
      button_hold_counts = 0;
    }
    if (button_hold_counts == BUTTON_HOLD_DURATION_MINIMUM) {
      button_state = HOLDING_STARTS;
      ignore_next_button_release = true;
    } else if (button_hold_counts > BUTTON_HOLD_DURATION_MINIMUM){
      button_state = HELD;
      ignore_next_button_release = true;
    } else if (button_released && !ignore_next_button_release) {
      button_state = PRESSED_AND_RELEASED;
    } else if (button_released && ignore_next_button_release) {
      button_state = NOTHING_PRESSED;
      ignore_next_button_release = false;
    } else {
      button_state = NOTHING_PRESSED;
    }
  }
  buttonState_time = millis();
}

void read_encoder(){
  aState = digitalRead(ENCODER_A_PIN);
  long current_millis = millis();
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (aState != aLastState){     
    // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
    if (digitalRead(ENCODER_B_PIN) != aState) { 
      encoder_counter --;
      if (screen == RPM_SCREEN){
        target_rpm -=5; 
        if (target_rpm< min_rpm) target_rpm = min_rpm;
      } else if (screen == MIN_SETTING_SCREEN){
        min_rpm -=5; 
        if (min_rpm < 20) min_rpm = 20; //TODO dinamic_absolute min_rpm, based on microstep
      } else if (screen == MAX_SETTING_SCREEN){
        max_rpm -=5;
        if (max_rpm < min_rpm) max_rpm = min_rpm;
      } else if (screen == MICROSTEP_SETTING_SCREEN){
        microstep/=2; if (microstep<16) microstep=16;
      } else if (screen == SETTING_SCREEN){
        setting_state --; 
        if (setting_state==0) setting_state=3;
      }
    } else {
      encoder_counter ++;
      if (screen == RPM_SCREEN){
        target_rpm +=5; 
        if (target_rpm> max_rpm) target_rpm = max_rpm;
      } else if (screen == MIN_SETTING_SCREEN){
        min_rpm +=5; 
        if (min_rpm > max_rpm) min_rpm = max_rpm; 
      } else if (screen == MAX_SETTING_SCREEN){
        max_rpm +=5;
        if (max_rpm > 500) max_rpm = 500; //TODO decide if it's ok or not
      } else if (screen == MICROSTEP_SETTING_SCREEN){
        microstep*=2; if (microstep>128) microstep=128;
      } else if (screen == SETTING_SCREEN){
        setting_state++; 
        if (setting_state==4) setting_state=1;
      }
    }
    data_changed = true;
    encoder_prev_t = current_millis;
  } 
  if (data_changed && current_millis - encoder_prev_t > 200){
    encoder_showing_disp = true;
  }
  aLastState = aState; // Updates the previous state of the outputA with the current state
}

void read_input_and_update_pwmHandler(){
  updateButtonSate(BUTTON1_PIN, button_hold_counts_1, button_state_1, ignore_next_button_release_1, buttonState_time_1);
  updateButtonSate(BUTTON2_PIN, button_hold_counts_2, button_state_2, ignore_next_button_release_2, buttonState_time_2);
  updateButtonSate(BUTTON3_PIN, button_hold_counts_3, button_state_3, ignore_next_button_release_3, buttonState_time_3);
  updateButtonSate(BUTTON4_PIN, button_hold_counts_4, button_state_4, ignore_next_button_release_4, buttonState_time_4);
  updateButtonSate(ENCODER_BUTTON_PIN, button_hold_counts_encoder, button_state_encode, ignore_next_button_release_encode, buttonState_time_encode);
  read_encoder();
}

//int test_freq = 2000;
bool up = true;

void loop() {  
  read_input_and_update_pwmHandler();//todo miki_style_button, encoder_4
  update_state();//todo miki_style_button, encoder_4
  show_display();//if required, todo
  //handle_motor_better();
}

void PWM_turn_off(){
  if (is_there_stop ) {
    motor_state = STOPPING;
    current_rpm = target_rpm;  //Not a good idea, since when we swich between stopping and starting there could be huge jumps
  } else {
    pinMode(PWM_PIN, OUTPUT);
    is_PWM_on = false;
  }
  /*pinMode(PWM_PIN, OUTPUT);
  is_PWM_on = false;*/
}

void PWM_turn_on(){
  pinMode(PWM_PIN, PWM);
  is_PWM_on = true;
  if (is_there_start) {
    motor_state = STARTING;
    current_rpm = min_rpm; 
  }
}

/* If max_rpm is 300 then that's 5 rps and 1000 fullstep per second
 * So we need the PWM signal to be microstep * 1000 H 
 * */
/* setting the global microstep lol */
void set_microstep(int mic){
  if (mic == 1 || mic == 2 || mic == 4 || mic == 8 || 
      mic == 16 || mic == 32 || mic == 64 || mic == 128){
    microstep = mic;
  }
}

/*
 * The user want's a specific rpm, and we gonna set the frequency of the PWM signal counting the 
 * parameters of the motor and the current microstep
 * if for some reason it's not possible it returns false
 */
bool set_rpm(int target_rpm_){
  double target_freq_ = 0;
  if (target_rpm_ <= max_rpm && target_rpm_ >= min_rpm){
    //rpm/max_rpm = freq/(microstep*one_round_number_off_step*(max_rpm/60));
    target_freq_ = target_rpm_  * microstep * STEP_PER_REVOLUTION * (1.0 / 60);
  } else {
    return false;
  }
  if (set_frequency(target_freq_)){
    PWM_by_HardverTimer = true; //We can use the PWM pin, we don't need to suffer
    warning_bool = false;
  } else {
    warning_bool = true;
    PWM_by_HardverTimer = false; //Our suffering shall commence! TODO: implement this other way
  }
  return true;
}

/* Returns false if you can't set that frequency, 
   you can't ask for greater than 65536 from the .setOverflow()*/
bool set_frequency(double target_freq_){ //This is not this simple
  int overflow_value = round(CPU_FREQ / target_freq_); 
  if (overflow_value > 65535) return false;
  pwmtimer.pause();
  pwmtimer.setPrescaleFactor(1);
  pwmtimer.setOverflow(overflow_value - 1);   
  pwmtimer.setCompare(TIMER_CH1, overflow_value / 2);  // 50% duty cycle
  pwmtimer.refresh();
  pwmtimer.resume();
  return true;
}