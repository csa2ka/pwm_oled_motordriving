#include <TaskScheduler.h>
#include <math.h>
#include <Arduino.h>
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

void refresh_display_task_Callback();
void read_input_task_Callback();

const int FPS = 4;
#define BUTTON1_PIN PB5
#define BUTTON2_PIN PB7
#define BUTTON3_PIN PB9
#define BUTTON4_PIN PB8

char buttonstate1[] = "UP";
char buttonstate2[] = "UP";
char buttonstate3[] = "UP";
char buttonstate4[] = "UP";

Task refresh_display_task(1000/FPS, TASK_FOREVER, &refresh_display_task_Callback);
Task read_input_task(1000/1, TASK_FOREVER, &read_input_task_Callback);
Scheduler runner;


const long SHORT_PRESS_TRESHOLD = 20;
const long LONG_PRESS_TRESHOLD = 2000;
/*const int ON = 1;
const int OFF = 2;
const int SHORT = 3;
const int LONG = 4;
const int SHORT_ENDED = 5;
const int LONG_ENDED = 6;*/
long button_1_press_start = 0;
int button_1_state = 1; //IDLE;
long button_2_press_start = 0;
int button_2_state = 1; //IDLE;
long button_3_press_start = 0;
int button_3_state = 1; //IDLE;
long button_4_press_start = 0;
int button_4_state = 1; //IDLE;

const int RPM_SCREEN = 1;
const int MICROSTEP_SETTING_SCREEN = 2;
const int MAX_SETTING_SCREEN = 3;
const int MIN_SETTING_SCREEN = 4;
//int _SETTING_SCREEN = 5;
int screen = RPM_SCREEN;
//int number = 1;
int start_x = 16;
int start_y = 62;
int delta = 32;
int RPM_TEXT_X = 77;//82;

#define PWM_PIN PA6
HardwareTimer pwmtimer(3);
#define DIRECTION_PIN PB1

int microstep = 128; //Should be between these: 1, 2, 4, 8, ... , 128

const long CPU_FREQ = 72000000; //Hertz 
const int STEP_PER_REVOLUTION = 200;
int max_rpm = 300; 
int min_rpm = 20;
int target_rpm = 120;

int current_rpm = 0;


const int STOPPED = 0;
const int STARTING = 1;
const int RUNNING = 2;
const int STOPPING = 3;
const int TURNING_STOPPING = 4;
const int TURNING_STARTING = 5;
int motor_state = STOPPED; 
bool is_there_stop = true;
bool is_there_start = true;
//bool starting = false;
//bool stopping = false;
bool is_PWM_on = false;
bool direction_bool = false;

void refresh_display_task_Callback() {
  switch (motor_state){
    case (STARTING):
      if (current_rpm == 0) current_rpm = min_rpm;
      if (current_rpm < target_rpm) {
        current_rpm = current_rpm * 13 / 10;
      }
      if (current_rpm > target_rpm) current_rpm = target_rpm;
    break;
    case (STOPPING):
      if (current_rpm > min_rpm) { //decrease current_rpm until min_rpm reached
        current_rpm = current_rpm * 10 / 13;
      } else {
        current_rpm = 0;
        pinMode(PWM_PIN, OUTPUT);  //should turn off onl if needed not always
        is_PWM_on = false;
      }
      break;
    case (TURNING_STOPPING):
      if (current_rpm > min_rpm) { //decrease current_rpm until min_rpm reached
        current_rpm = current_rpm * 10/13;
      } else {
        current_rpm = min_rpm;
        direction_bool = !direction_bool;
        if (direction_bool) digitalWrite(DIRECTION_PIN, HIGH);
        else digitalWrite(DIRECTION_PIN, LOW);
        motor_state = STARTING;
      }
    break;
  }
  set_rpm(current_rpm);
  show_display();
}



void read_input_task_Callback(){
  read_button(BUTTON1_PIN, button_1_press_start, button_1_state);
  read_button(BUTTON2_PIN, button_2_press_start, button_2_state);
  read_button(BUTTON3_PIN, button_3_press_start, button_3_state);
  read_button(BUTTON4_PIN, button_4_press_start, button_4_state);
  update_state();
}


const int IDLE = 1;
const int SHORT_PRESS = 2;
const int JUST_ENTERED_LONG_PRESS = 3;
const int JUST_LEFT_SHORT_PRESS = 4;
const int LONG_PRESS = 5;

int read_button(int BUTTON_PIN, long &button_press_start, int &button_state){
  long current_millis = millis();
  bool button_pressed = false;
  if (digitalRead(BUTTON_PIN) == LOW) button_pressed = true;
  /*

    We want to act when we leave shortpress and when we enter longpress
    needed states:
    
  */
  switch (button_state){
    case IDLE:
      if (button_pressed) {
        button_state = SHORT_PRESS;
        button_press_start = current_millis;
      }
      break;
    case SHORT_PRESS:
      if (button_pressed) {
        if (current_millis - button_press_start > LONG_PRESS_TRESHOLD) button_state = JUST_ENTERED_LONG_PRESS;
      } else {
        /*if (current_millis - button_press_start > SHORT_PRESS_TRESHOLD)*/ button_state = JUST_LEFT_SHORT_PRESS;
      }
      break;
    case JUST_ENTERED_LONG_PRESS:
      if (button_pressed) {
        button_state = LONG_PRESS;
      } else {
        button_state = IDLE;
      }
      break;
    case JUST_LEFT_SHORT_PRESS:
      if (button_pressed) {
        button_state = SHORT_PRESS;
      } else {
        button_state = IDLE;
      }
      break;
    case LONG_PRESS:
      if (button_pressed) {
      } else {
        button_state = IDLE;
      }
      break;
  }
}


void PWM_turn_off(){
  if (is_there_stop ) {
    motor_state = STOPPING;
    //current_rpm = target_rpm;  //Not a good idea, since when we swich between stopping and starting there could be huge jumps
  } else {
    pinMode(PWM_PIN, OUTPUT);
    is_PWM_on = false;
  }
}

void PWM_turn_on(){
  pinMode(PWM_PIN, PWM);
  is_PWM_on = true;
  if (is_there_start) {
    motor_state = STARTING;
    current_rpm = min_rpm; 
  }
}

/* If max_rpm is 300 then that's 5 rps and 1000 fullstep per second
 * So we need the PWM signal to be microstep * 1000 H 
 * */
/* setting the global microstep lol */
void set_microstep(int mic){
  if (mic == 1 || mic == 2 || mic == 4 || mic == 8 || 
      mic == 16 || mic == 32 || mic == 64 || mic == 128){
    microstep = mic;
  }
}

double target_freq; //what is the frequency we want to send the PWM to the motor
bool PWM_by_HardverTimer; //Can we use the HardverTimer, or we need to use an other method
/*
 * The user want's a specific rpm, and we gonna set the frequency of the PWM signal counting the 
 * parameters of the motor and the current microstep
 * if for some reason it's not possible it returns false
 */
bool warning_bool = false;
bool set_rpm(int target_rpm){
  if (target_rpm <= max_rpm && target_rpm >= min_rpm){
    //rpm/max_rpm = freq/(microstep*one_round_number_off_step*(max_rpm/60));
    target_freq = ((target_rpm * 1.0) / (max_rpm)) * (microstep * STEP_PER_REVOLUTION * (max_rpm / 60));
  } else {
    return false;
  }
  if (set_frequency(target_freq)){
    PWM_by_HardverTimer = true; //We can use the PWM pin, we don't need to suffer
    warning_bool = false;
  } else {
    warning_bool = true;
    PWM_by_HardverTimer = false; //Our suffering shall commence! TODO: implement this other way
  }
  return true;
}

/* Returns false if you can't set that frequency, 
   you can't ask for greater than 65536 from the .setOverflow()*/
bool set_frequency(double target_freq){ //This is not this simple
  int overflow_value = round(CPU_FREQ / target_freq); 
  if (overflow_value > 65535) return false;
  pwmtimer.pause();
  pwmtimer.setPrescaleFactor(1);
  pwmtimer.setOverflow(overflow_value - 1);   
  pwmtimer.setCompare(TIMER_CH1, overflow_value / 2);  // 50% duty cycle
  pwmtimer.refresh();
  pwmtimer.resume();
  return true;
}

void show_display(){
  u8g2.clearBuffer();					// clear the internal memory
  if (screen == RPM_SCREEN){  
    draw_number_screen("RPM", RPM_TEXT_X-start_x, target_rpm, true);
  } else if(screen == MICROSTEP_SETTING_SCREEN){
    draw_number_screen("MICROSTEP", 0, microstep, false);
  } else if(screen == MAX_SETTING_SCREEN){
    draw_number_screen("MAX RPM", 0, max_rpm, false);
  } else if(screen == MIN_SETTING_SCREEN){
    draw_number_screen("MIN RPM", 0, min_rpm, false);
  }
  u8g2.sendBuffer();
}

void draw_number_screen(char * heading, int addition, int number, bool is_main_screen){
  u8g2.setFont(u8g2_font_7Segments_26x42_mn);
    if (number > 99){
      u8g2.drawStr(start_x, start_y, u8x8_utoa(number));
    } else if (number > 9){
      u8g2.drawStr(start_x + delta, start_y, u8x8_utoa(number));
    } else {
      u8g2.drawStr(start_x + delta + delta, start_y, u8x8_utoa(number));  
    }
    u8g2.setFont(u8g2_font_logisoso16_tf);//u8g2_font_freedoomr10_mu  );
    if (is_main_screen){
      u8g2.drawStr(start_x , 16, u8x8_utoa(current_rpm));
      if (direction_bool) u8g2.drawTriangle(1,38,9,38,5,30);
      else u8g2.drawTriangle(1,42,9,42,5,50);
      if (warning_bool) u8g2.drawStr(0, 16, "W"); 
    } 	// write something to the internal memory
    u8g2.drawStr(start_x + addition, 16, heading);	// write something to the internal memory
     /*
      u8g2_font_logisoso16_tf
      u8g2_font_freedoomr10_mu
    */
}


void update_state(){
  switch(screen) {
    case RPM_SCREEN:
      if (button_2_state == JUST_ENTERED_LONG_PRESS) {
        if (is_PWM_on) {
          motor_state = TURNING_STOPPING;  
        } else {
          direction_bool = !direction_bool;
          if (direction_bool) digitalWrite(DIRECTION_PIN, HIGH);
          else digitalWrite(DIRECTION_PIN, LOW);
        }
      }
      if (button_2_state == JUST_LEFT_SHORT_PRESS) {
        if (!is_PWM_on) PWM_turn_on();
        else PWM_turn_off();
      }
      if (button_3_state == JUST_LEFT_SHORT_PRESS) target_rpm=target_rpm+5; if (target_rpm>max_rpm) target_rpm=max_rpm;
      if (button_4_state == JUST_LEFT_SHORT_PRESS) target_rpm=target_rpm-5; if (target_rpm<min_rpm) target_rpm=min_rpm;
      //if (button_2_state == SHORT_ENDED) if (is_PWM_on) PWM_turn_off; else PWM_turn_on;
      if (button_3_state == JUST_ENTERED_LONG_PRESS) screen = MIN_SETTING_SCREEN;
      if (button_4_state == JUST_ENTERED_LONG_PRESS) screen = MICROSTEP_SETTING_SCREEN;
      break;
    case MICROSTEP_SETTING_SCREEN:
      PWM_turn_off();
      if (button_3_state == JUST_LEFT_SHORT_PRESS) microstep*=2; if (microstep>128) microstep=128;
      if (button_4_state == JUST_LEFT_SHORT_PRESS) microstep/=2; if (microstep<16) microstep=16;
      if (button_3_state == JUST_ENTERED_LONG_PRESS) screen = RPM_SCREEN;
      if (button_4_state == JUST_ENTERED_LONG_PRESS) screen = MAX_SETTING_SCREEN;
      break;
    case MAX_SETTING_SCREEN:
      PWM_turn_off();
      if (button_3_state == JUST_LEFT_SHORT_PRESS) max_rpm+=5; if (max_rpm>500) max_rpm=350;
      if (button_4_state == JUST_LEFT_SHORT_PRESS) max_rpm-=5; if (max_rpm<200) max_rpm=200;
      if (button_3_state == JUST_ENTERED_LONG_PRESS) screen = MICROSTEP_SETTING_SCREEN;
      if (button_4_state == JUST_ENTERED_LONG_PRESS) screen = MIN_SETTING_SCREEN;
      break;
    case MIN_SETTING_SCREEN:
      PWM_turn_off();
      if (button_3_state == JUST_LEFT_SHORT_PRESS) min_rpm+=5; if (min_rpm>200) min_rpm=200;
      if (button_4_state == JUST_LEFT_SHORT_PRESS) min_rpm-=5; if (min_rpm<0) min_rpm=0;
      if (button_3_state == JUST_ENTERED_LONG_PRESS) screen = MAX_SETTING_SCREEN;
      if (button_4_state == JUST_ENTERED_LONG_PRESS) screen = RPM_SCREEN;
      break;
  }
}

long test_start_t;
void setup() 
{
  u8g2.begin();
  runner.init();
  runner.addTask(refresh_display_task);
  //runner.addTask(read_input_task);
  delay(1000);
  refresh_display_task.enable();
  //read_input_task.enable();

  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  pinMode(BUTTON3_PIN, INPUT_PULLUP);
  pinMode(BUTTON4_PIN, INPUT_PULLUP);
  pinMode(DIRECTION_PIN, OUTPUT);
}


//int test_freq = 2000;
bool up = true;
void loop() {
  read_button(BUTTON1_PIN, button_1_press_start, button_1_state);
  read_button(BUTTON2_PIN, button_2_press_start, button_2_state);
  read_button(BUTTON3_PIN, button_3_press_start, button_3_state);
  read_button(BUTTON4_PIN, button_4_press_start, button_4_state);
  update_state();
  runner.execute();
}
