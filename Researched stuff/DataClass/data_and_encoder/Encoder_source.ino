#include "Encoder_header.h"
#include "All_data.h"
#include <stdbool.h>


encoder makeEncoder(int out_A, int out_B, struct all_data * a_d, void (*up)(struct all_data *), void (*down)(struct all_data *)){
    encoder enci;
    enci.data = a_d;
    enci.up = up;
    enci.down = down;
    enci.outputA = out_A;
    enci.outputB = out_B;
    enci.counter = 0;
    enci.upper_bound;
    enci.lower_bound;
    enci.aState;
    enci.aLastState;
    enci.data_changed = false;
    enci.showing_disp = true;
    enci.prev_t = 0;
    enci.last_time_display_was_shown = 0;
    pinMode (enci.outputA,INPUT_PULLUP);
    pinMode (enci.outputB,INPUT_PULLUP);
    // Reads the initial state of the outputA
    enci.aLastState = digitalRead(enci.outputA);   
    return enci;
}

void read_encoder(struct encoder *enci){
  int ret = 0;
  enci->aState = digitalRead(enci->outputA);
  long current_millis = millis();
  if (enci->aState != enci->aLastState){     
    if (digitalRead(enci->outputB) != enci->aState) { 
      enci->counter ++;
      handle_rotation(enci, 1);
      (*enci->up)(enci->data);
    } else {
      enci->counter --;
      handle_rotation(enci, -1);
      (*enci->down)(enci->data);
    }
    enci->data_changed = true;
    enci->prev_t = current_millis;
  } 
  if (enci->data_changed && current_millis - enci-> prev_t > 200 /*&& prev_t - prev_prev_t > 200*/){
    enci->showing_disp = true;
  }
  enci->aLastState = enci->aState;
}

bool encoder_request_displaying(struct encoder *enci){
    return enci->showing_disp || ((millis() - enci->last_time_display_was_shown > 500 && enci->data_changed));
}

void reset_encoder_before_display(struct encoder *enci){
    enci->last_time_display_was_shown = millis();
}

void reset_encoder_after_display(struct encoder *enci){
    enci->aLastState = digitalRead(enci->outputA);
    enci->showing_disp = false;
    enci->data_changed = false;
}

int handle_rotation(struct encoder *enci, int increment){
  enci->counter += increment;
}
