#ifndef ENCODER_H_INCLUDED
#define ENCODER_H_INCLUDED
/* ^^ these are the include guards */

/* Prototypes for the functions */
/* Sums two ints */

typedef struct encoder {
    struct all_data * data;
    void (*up)(struct all_data *) ;
    void (*down)(struct all_data *) ;
    int outputA;
    int outputB;
    int counter;
    int upper_bound;
    int lower_bound;
    int aState;
    int aLastState;
    bool data_changed;
    bool showing_disp;
    long prev_t;
    long last_time_display_was_shown;
    long display_waiting;
} encoder; 

encoder makeEncoder(int out_A, int out_B, struct all_data * data, void (*up)(struct all_data *), void (*down)(struct all_data *));

void read_encoder(struct encoder *enci);

bool encoder_request_displaying(struct encoder *enci);

void reset_encoder_before_display(struct encoder *enci);

void reset_encoder_after_display(struct encoder *enci);

#endif