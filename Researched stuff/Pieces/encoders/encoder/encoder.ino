//INTERRUPTS IN STM32F103C8
//CIRCUIT DIGEST
#include <Arduino.h>
#include <U8g2lib.h>
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
#include <TaskScheduler.h>

void t1Callback();

Task t1(1000, TASK_FOREVER, &t1Callback);
Scheduler runner;

#define ENCODER_PIN_1 PB14
#define ENCODER_PIN_2 PB13

//nyomó: 12 encoder: 13-14

volatile int p1 = 0;
volatile int p2 = 0;

volatile int p1_c = 0;
volatile int p2_c = 0;

void setup(){
  u8g2.begin();                                              
  
  pinMode(LED_BUILTIN,OUTPUT);                                           
  pinMode(ENCODER_PIN_1, INPUT_PULLUP);          
  pinMode(ENCODER_PIN_2, INPUT_PULLUP);                                               
  digitalWrite(LED_BUILTIN, LOW);
  attachInterrupt(digitalPinToInterrupt(ENCODER_PIN_1), encoder_1_Rotate, CHANGE);                   
  attachInterrupt(digitalPinToInterrupt(ENCODER_PIN_2), encoder_2_Rotate, CHANGE);

  runner.init();
  runner.addTask(t1);
  delay(1000);
  t1.enable();
}


void loop()                                                       //  void loops runs continuously
{  
  /*p1_c = digitalReadN(ENCODER_PIN_1, 10);
  p2_c = digitalReadN(ENCODER_PIN_2, 10);*/
  runner.execute();                                              //  delays time 
}

//

volatile const long CHANGE_REGISTERING_COOLDOWN = 200;
volatile long last_button_change_t_1;
volatile long last_button_change_t_2;      
volatile const long PRESS_COOLDOWN = 250;  
volatile long last_button_press_t;
volatile int treshold = 2;
//volatile long last_button_press_t;

volatile long encoder_counter = 1000;
volatile int counter_all = 0;
volatile int counter_in = 0;


int digitalReadN(int pin, int n){
  int low_counter = 0;
  int high_counter = 0;
  for (int i =0; i < n;  i++){
    if (digitalRead(pin) == HIGH) high_counter++; else low_counter ++; 
  }
  if (low_counter > high_counter) return LOW; else return HIGH;
}

const int CLOCKWISE = 0;
const int COUNTER_CLOCKWISE = 1;
int direction_1 = CLOCKWISE;
int direction_2 = CLOCKWISE;

int clockwise_counter = 0;
int counter_clockwise_counter = 0;

void encoder_1_Rotate()                                              //
{                 
  //counter_all ++;
  long current_millis = millis();
  //delay(1);  //so when we digitalRead() we can be sure what's up
  if (current_millis - last_button_press_t > PRESS_COOLDOWN){
    if ( current_millis - last_button_change_t_1 > CHANGE_REGISTERING_COOLDOWN){
      //counter_in ++;
      //p1 = digitalReadN(ENCODER_PIN_1,10);
      //p2 = digitalReadN(ENCODER_PIN_2,10);
      p1 = digitalReadN(ENCODER_PIN_1, 100);
      p2 = digitalReadN(ENCODER_PIN_2, 100);
      if (p1 == p2){
        //encoder_counter ++;
        direction_1 = CLOCKWISE;
        clockwise_counter ++;
        counter_clockwise_counter = 0;
        if (clockwise_counter>=treshold){
          clockwise_counter = 0;
          encoder_counter ++;
          last_button_press_t = current_millis; 
        }
      } else {
        //encoder_counter --;
        direction_1 = COUNTER_CLOCKWISE;
        counter_clockwise_counter ++;
        clockwise_counter = 0;
        if (counter_clockwise_counter>=treshold){
          counter_clockwise_counter = 0;
          encoder_counter --;
          last_button_press_t = current_millis; 
        }
      }
      last_button_change_t_1 = current_millis;   
    }
  }
}

void encoder_2_Rotate()                                              //
{                 
  //counter_all ++;
  long current_millis = millis();
  //delay(1);  //so when we digitalRead() we can be sure what's up
  if (current_millis - last_button_press_t > PRESS_COOLDOWN){
    if ( current_millis - last_button_change_t_2 > CHANGE_REGISTERING_COOLDOWN){
      //counter_in ++;
      //p1 = digitalReadN(ENCODER_PIN_1,10);
      //p2 = digitalReadN(ENCODER_PIN_2,10);
      p1 = digitalReadN(ENCODER_PIN_1, 100);
      p2 = digitalReadN(ENCODER_PIN_2, 100);
      if (p1 != p2){
        //encoder_counter ++;
        direction_1 = CLOCKWISE;
        clockwise_counter ++;
        counter_clockwise_counter = 0;
        if (clockwise_counter>=treshold){
          clockwise_counter = 0;
          encoder_counter ++;
          last_button_press_t = current_millis;
        }
      } else {
        //encoder_counter --;
        direction_1 = COUNTER_CLOCKWISE;
        counter_clockwise_counter ++;
        clockwise_counter = 0;
        if (counter_clockwise_counter>=treshold){
          counter_clockwise_counter = 0;
          encoder_counter --;
          last_button_press_t = current_millis;
        }
        last_button_change_t_2 = current_millis;   
      }
    }
  }
}

void show_display(){ 
  int y=20;
  noInterrupts();
  //delay(1);
  int n1 = encoder_counter;
  /*int n2 = counter_all;
  int n3 = counter_in;
  int p_1 = p1;
  int p_2 = p2;
  int p_1_c = p1_c;
  int p_2_c = p2_c;*/
  interrupts();
  u8g2.clearBuffer();         // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(5, 20, u8x8_utoa(n1));
  /*u8g2.drawStr(5, 40, u8x8_utoa(n2));
  u8g2.drawStr(5, 60, u8x8_utoa(n3));
  u8g2.drawStr(50, 40, u8x8_utoa(p_1));
  u8g2.drawStr(50, 60, u8x8_utoa(p_2));
  u8g2.drawStr(70, 40, u8x8_utoa(p_1_c));
  u8g2.drawStr(70, 60, u8x8_utoa(p_2_c));*/
  //u8g2.drawStr(50, 14, u8x8_utoa(state));
  u8g2.sendBuffer();
}

void t1Callback() {
  show_display();
}