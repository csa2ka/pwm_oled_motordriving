#include <Arduino.h>
#include <U8g2lib.h>
#include "Encoder_header.h"
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE); 
 
 #define output__A PB13
 #define output__B PB14
 struct encoder the_encoder;

 void setup() {
  u8g2.begin();    
  the_encoder = makeEncoder(output__A, output__B);
 } 

int a;
void right(){
  a++;
}

void left(){
  a--;
}

void loop() { 
  read_encoder(&the_encoder, right, left);
  if (encoder_request_displaying(&the_encoder)){
    show_display();
  }
}

void show_display(){ 
  reset_encoder_before_display(&the_encoder); 
  int y=20;
  noInterrupts();
  int n1 = the_encoder.counter;
  interrupts();
  u8g2.clearBuffer();         // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(20, 20, u8x8_utoa(n1));
  u8g2.drawStr(20, 40, u8x8_utoa(a));
  u8g2.sendBuffer();
  reset_encoder_after_display(&the_encoder);
}
