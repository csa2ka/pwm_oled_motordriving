 /*
  Rotary Encoder Demo
  rot-encode-demo.ino
  Demonstrates operation of Rotary Encoder
  Displays results on Serial Monitor
  DroneBot Workshop 2019
  https://dronebotworkshop.com
*/
 #include <Arduino.h>
#include <U8g2lib.h>
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
#include <TaskScheduler.h>
 
void t1Callback();
void t2Callback();

Task t1(300, TASK_FOREVER, &t1Callback);
Task t2(1, TASK_FOREVER, &t2Callback);
Scheduler runner;

void task_and_display_setup(){
  u8g2.begin();                                        
  runner.init();
  runner.addTask(t1);
  runner.addTask(t2);
  delay(1000);
  t1.enable();
  t2.enable();
}




void t2Callback() {
  
}

void t1Callback() {
  //show_display();
}



/*******Interrupt-based Rotary Encoder Sketch*******
by Simon Merrett, based on insight from Oleg Mazurov, Nick Gammon, rt, Steve Spence
*/

static int pinA = PB13; // Our first hardware interrupt pin is digital pin 2
static int pinB = PB14; // Our second hardware interrupt pin is digital pin 3
volatile byte aFlag = 0; // let's us know when we're expecting a rising edge on pinA to signal that the encoder has arrived at a detent
volatile byte bFlag = 0; // let's us know when we're expecting a rising edge on pinB to signal that the encoder has arrived at a detent (opposite direction to when aFlag is set)
volatile byte encoderPos = 0; //this variable stores our current value of encoder position. Change to int or uin16_t instead of byte if you want to record a larger range than 0-255
volatile byte oldEncPos = 0; //stores the last encoder position value so we can compare to the current reading and see if it has changed (so we know when to print to the serial monitor)
volatile byte reading = 0; //somewhere to store the direct values we read from our interrupt pins before checking to see if we have moved a whole detent

void setup() {
  task_and_display_setup();
  pinMode(pinA, INPUT_PULLUP); // set pinA as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  pinMode(pinB, INPUT_PULLUP); // set pinB as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  attachInterrupt(digitalPinToInterrupt(pinA),PinA,RISING); // set an interrupt on PinA, looking for a rising edge signal and executing the "PinA" Interrupt Service Routine (below)
  attachInterrupt(digitalPinToInterrupt(pinB),PinB,RISING); // set an interrupt on PinB, looking for a rising edge signal and executing the "PinB" Interrupt Service Routine (below)
  Serial.begin(115200); // start the serial monitor link
}

int counterA = 0;
void PinA(){
  noInterrupts(); //stop interrupts happening before we read pin values
  counterA++;
  //reading = PIND & 0xC; // read all eight pin values then strip away all but pinA and pinB's values
  //if(reading == B00001100 && aFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
  int pin1 = digitalRead(pinA);
  int pin2 = digitalRead(pinB);
  if (pin1 == HIGH && pin2 == HIGH && aFlag){
    encoderPos --; //decrement the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (pin1 == HIGH && pin2 == HIGH) bFlag = 1; //signal that we're expecting pinB to signal the transition to detent from free rotation
  interrupts(); //restart interrupts
}

int counterB = 0;
void PinB(){
  noInterrupts(); //stop interrupts happening before we read pin values
  counterB ++;
  //reading = PIND & 0xC; //read all eight pin values then strip away all but pinA and pinB's values
  //if (reading == B00001100 && bFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
  int pin1 = digitalRead(pinA);
  int pin2 = digitalRead(pinB);
  if (pin1 == HIGH && pin2 == HIGH && bFlag){
    encoderPos ++; //increment the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (pin1 == HIGH && pin2 == HIGH) aFlag = 1; //signal that we're expecting pinA to signal the transition to detent from free rotation
  interrupts(); //restart interrupts
}

void loop(){
  if(oldEncPos != encoderPos) {
    Serial.println(encoderPos);
    oldEncPos = encoderPos;
    show_display();
  }
  runner.execute();
}



void show_display(){ 

  int y=20;
  //noInterrupts();
  int n1 = encoderPos;
  int n2 = counterA;
  int n3 = counterB;
  //int n2 = encoder0Pos;
  /*int n3 = counter_in;
  int p_1 = p1;
  int p_2 = p2;*/
  //interrupts();
  u8g2.clearBuffer();         // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(5, 20, u8x8_utoa(n1));
  u8g2.drawStr(5, 40, u8x8_utoa(n2));
  u8g2.drawStr(50, 20, u8x8_utoa(n3));
  //u8g2.drawStr(5, 40, u8x8_utoa(n2));
  /*u8g2.drawStr(50, 20, u8x8_utoa(digitalRead(inputCLK)));
  u8g2.drawStr(50, 40, u8x8_utoa(digitalRead(inputDT)));
  u8g2.drawStr(5, 65, u8x8_utoa(delta_t));*/
  /*u8g2.drawStr(5, 60, u8x8_utoa(n3));
  u8g2.drawStr(50, 40, u8x8_utoa(p_1));
  u8g2.drawStr(50, 60, u8x8_utoa(p_2));*/
  //u8g2.drawStr(50, 14, u8x8_utoa(state));
  u8g2.sendBuffer();
}