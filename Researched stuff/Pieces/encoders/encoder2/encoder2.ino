 /*
  Rotary Encoder Demo
  rot-encode-demo.ino
  Demonstrates operation of Rotary Encoder
  Displays results on Serial Monitor
  DroneBot Workshop 2019
  https://dronebotworkshop.com
*/
 #include <Arduino.h>
#include <U8g2lib.h>
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
#include <TaskScheduler.h>
 // Rotary Encoder Inputs
 #define inputCLK PB13
 #define inputDT PB14
 
void t1Callback();
void t2Callback();

Task t1(300, TASK_FOREVER, &t1Callback);
Task t2(1, TASK_FOREVER, &t2Callback);
Scheduler runner;


 int counter = 0; 
 int currentStateCLK;
 int previousStateCLK; 

 int encdir =1;

 void setup() { 
   
   // Set encoder pins as inputs  
  pinMode (inputCLK,INPUT_PULLUP);
  pinMode (inputDT,INPUT_PULLUP);
   
  pinMode( LED_BUILTIN, OUTPUT);
  // Read the initial state of inputCLK
  // Assign to previousStateCLK variable
  previousStateCLK = digitalRead(inputCLK);
  u8g2.begin();                                              
  

  runner.init();
  runner.addTask(t1);
  runner.addTask(t2);
  delay(1000);
  t1.enable();
  t2.enable();
}

int count1h = 0;
int count1l = 0;
int count2h = 0;
int count2l = 0;

void readInputUpdateCounters(){
   if (digitalRead(inputCLK) == HIGH){
     count1h ++;
     count1l = 0;
   } else {
     count1h = 0;
     count1l ++;
   }
   if (digitalRead(inputDT) == HIGH){
     count2h ++;
     count2l = 0;
   } else {
     count2h = 0;
     count2l ++;
   }
}

void encoder_Read(int compare_this, int to_that, int & current_state, int & prev_state){
    current_state = digitalRead(compare_this);
   // If the previous and the current state of the inputCLK are different then a pulse has occured
   if ((current_state != prev_state)){  
     // If the inputDT state is different than the inputCLK state then 
     // the encoder is rotating counterclockwise
     if (digitalRead(to_that) != current_state) { 
       counter --;
       encdir = 1; 
     } else {
       // Encoder is rotating clockwise
       counter ++;
       encdir = 0;
     }
   }
   // Update previousStateCLK with the current state
   //digitalWrite(LED_BUILTIN, encdir);
   prev_state = current_state;
}



int treshold = 1000;
volatile long delta_t = 0;
long prev_curr_t = 0;
long previous_not_not_ignored_input_time = 0;
long ignor_input_time = 500; 

void loop() { 
    long current_millis = millis();
    prev_curr_t = current_millis;
    delta_t = current_millis - prev_curr_t;
    if (current_millis - previous_not_not_ignored_input_time > ignor_input_time ){
      encoder_Read(inputCLK, inputDT, currentStateCLK, previousStateCLK);
      previous_not_not_ignored_input_time = current_millis;
    }
   /*// Read the current state of inputCLK
   currentStateCLK = digitalRead(inputCLK);
   // If the previous and the current state of the inputCLK are different then a pulse has occured
   if ((currentStateCLK != previousStateCLK)){  
     // If the inputDT state is different than the inputCLK state then 
     // the encoder is rotating counterclockwise
     if (digitalRead(inputDT) != currentStateCLK) { 
       counter --;
       encdir =1;
       
     } else {
       // Encoder is rotating clockwise
       counter ++;
       encdir =0;
     }
   } 
   // Update previousStateCLK with the current state
   //digitalWrite(LED_BUILTIN, encdir);
   previousStateCLK = currentStateCLK; */
   runner.execute();
 }
 


void show_display(){ 

  int y=20;
  //noInterrupts();
  int n1 = counter;
  int n2 = encdir;
  /*int n3 = counter_in;
  int p_1 = p1;
  int p_2 = p2;*/
  //interrupts();
  u8g2.clearBuffer();         // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(5, 20, u8x8_utoa(n1));
  u8g2.drawStr(5, 40, u8x8_utoa(n2));
  u8g2.drawStr(50, 20, u8x8_utoa(digitalRead(inputCLK)));
  u8g2.drawStr(50, 40, u8x8_utoa(digitalRead(inputDT)));
  u8g2.drawStr(5, 65, u8x8_utoa(delta_t));
  /*u8g2.drawStr(5, 60, u8x8_utoa(n3));
  u8g2.drawStr(50, 40, u8x8_utoa(p_1));
  u8g2.drawStr(50, 60, u8x8_utoa(p_2));*/
  //u8g2.drawStr(50, 14, u8x8_utoa(state));
  u8g2.sendBuffer();
}


void t2Callback() {
  
}

void t1Callback() {
  show_display();
}