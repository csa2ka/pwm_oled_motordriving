#include <math.h>

#define PWM_PIN PA6
HardwareTimer pwmtimer(3);

void PWM_turn_off(){
  pinMode(PWM_PIN, OUTPUT);
}

void PWM_turn_on(){
  pinMode(PWM_PIN, PWM);
}

int microstep = 128; //Should be between these: 1, 2, 4, 8, ... , 128

const long CPU_FREQ = 72000000; //Hertz 
const int STEP_PER_REVOLUTION = 200;
int max_rpm = 300; 
int min_rpm = 20;
/* If max_rpm is 300 then that's 5 rps and 1000 fullstep per second
 * So we need the PWM signal to be microstep * 1000 H 
 * */
/* setting the global microstep lol */
void set_microstep(int mic){
  if (mic == 1 || mic == 2 || mic == 4 || mic == 8 || 
      mic == 16 || mic == 32 || mic == 64 || mic == 128){
    microstep = mic;
  }
}

double target_freq; //what is the frequency we want to send the PWM to the motor
bool PWM_by_HardverTimer; //Can we use the HardverTimer, or we need to use an other method
/*
 * The user want's a specific rpm, and we gonna set the frequency of the PWM signal counting the 
 * parameters of the motor and the current microstep
 * if for some reason it's not possible it returns false
 */
bool set_rpm(int target_rpm){
  if (target_rpm <= max_rpm && target_rpm >= min_rpm){
    //rpm/max_rpm = freq/(microstep*one_round_number_off_step*(max_rpm/60));
    target_freq = ((target_rpm * 1.0) / (max_rpm)) * (microstep * STEP_PER_REVOLUTION * (max_rpm / 60));
  } else {
    return false;
  }
  if (set_frequency(target_freq)){
    PWM_by_HardverTimer = true; //We can use the PWM pin, we don't need to suffer
  } else {
    PWM_by_HardverTimer = false; //Our suffering shall commence! TODO: implement this other way
  }
  return true;
}

/* Returns false if you can't set that frequency, 
   you can't ask for greater than 65536 from the .setOverflow()*/
bool set_frequency(double target_freq){ //This is not this simple
  int overflow_value = round(CPU_FREQ / target_freq); 
  if (overflow_value > 65535) return false;
  pwmtimer.pause();
  pwmtimer.setPrescaleFactor(1);
  pwmtimer.setOverflow(overflow_value - 1);   
  pwmtimer.setCompare(TIMER_CH1, overflow_value / 2);  // 50% duty cycle
  pwmtimer.refresh();
  pwmtimer.resume();
  return true;
}

void setup() 
{
  PWM_turn_on();
}

int target_rpm = 50;
int test_freq = 2000;
bool up = true;
void loop() {
  /*PWM_turn_off();
  delay(1000);
  PWM_turn_on();
  delay(1000);*/
  /*set_frequency(test_freq);
  if (up){
    if (test_freq<5000) test_freq = test_freq * 1.01;
    else up = false;
  } else {
    if (test_freq>500)  test_freq = test_freq * 0.99;
    else up = true;
  }*/
  set_rpm(target_rpm);
  if (up){
    if (target_rpm < max_rpm) target_rpm += 2;
    else up = false;
  } else {
    if (target_rpm > min_rpm)  target_rpm -= 2;
    else up = true;
  }
  delay(100);
}
