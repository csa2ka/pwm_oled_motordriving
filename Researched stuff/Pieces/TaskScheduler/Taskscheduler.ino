#include <TaskScheduler.h>

void tCallback();

Task t(3000, TASK_FOREVER, &tCallback);
Scheduler runner;

void tCallback() {
   digitalWrite(LED_BUILTIN, LOW); 
   delay(100);
   digitalWrite(LED_BUILTIN, HIGH);
}


void setup () {
  pinMode(LED_BUILTIN, OUTPUT);
  runner.init();
  runner.addTask(t);
  delay(1000);
  t.enable();;
}

void loop () {
  runner.execute();
}
