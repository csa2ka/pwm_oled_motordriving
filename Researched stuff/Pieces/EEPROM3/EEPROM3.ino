#include <EEPROM.h>
//#include <Arduino.h>
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

void show_text(char * text){
  u8g2.clearBuffer();					// clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(16, 16, text); 
  u8g2.sendBuffer();
}

void show_char(char bite){
  int i = bite;
  u8g2.clearBuffer();					// clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(16, 16, u8x8_utoa(i)); 
  u8g2.sendBuffer();
}

char * string2CharStar(String s){
  //String str("...");
  char str_array[s.length()];
  s.toCharArray(str_array, s.length());
  char* token = strtok(str_array, " ");
  return token;
}

void setup(){
  u8g2.begin();
  delay(100);
} 

void loop()
{
  show_text("Writing...");
  delay(1000);
  
  //EEPROM.write(0,240);

  show_text("Done writing");
  delay(1000);
  show_text("Reading...");
  delay(1000);

  show_char(EEPROM.read(0));
  
  delay(2000);
  show_text("Done reading");
}