HardwareTimer pwmtimer(3);
void setup() 
{
  pinMode(PA6, PWM);
  pwmtimer.pause();
  pwmtimer.setPrescaleFactor(1);
  pwmtimer.setOverflow(65536 - 1); // 72MHZ / 1200 = 60KHZ
  pwmtimer.setCompare(TIMER_CH1, 20000);  // 50% duty cycle
  pwmtimer.refresh();
  pwmtimer.resume();
}

void loop() {
  
}
