//INTERRUPTS IN STM32F103C8
//CIRCUIT DIGEST
#include <Arduino.h>
#include <U8g2lib.h>
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
#include <TaskScheduler.h>

void t1Callback();
void t2Callback();

Task t1(1000/3, TASK_FOREVER, &t1Callback);
Task t2(1000/1000, TASK_FOREVER, &t2Callback);
Scheduler runner;

const int IDLE = 1;
const int SHORT_PRESS = 2;
const int JUST_ENTERED_LONG_PRESS = 3;
const int JUST_LEFT_SHORT_PRESS = 4;
const int LONG_PRESS = 5;

const int LONG_PRESS_TRESHOLD = 1000;

int button_state = IDLE;
int state = 0;

volatile boolean ledOn = false;                                   //  variable declared as global
volatile int i = 0;
void setup(){
  u8g2.begin();                                                 //  delay time 
  
  pinMode(LED_BUILTIN,OUTPUT);                                            //  set pin PA1 as output
  pinMode(PB8,INPUT_PULLUP);          
  pinMode(PB9,INPUT_PULLUP);                                               //  declare variable i and initiliaze with 0  
  digitalWrite(LED_BUILTIN, LOW);
  attachInterrupt(digitalPinToInterrupt(PB8),buttonPressed,FALLING);                      //  function for creating external interrupts
  attachInterrupt(digitalPinToInterrupt(PB9),countingRising,RISING);

  runner.init();
  runner.addTask(t1);
  runner.addTask(t2);
  delay(1000);
  t1.enable();
  t2.enable();
}


void loop()                                                       //  void loops runs continuously
{  
  runner.execute();                                              //  delays time 
}

const long PRESS_REGISTERING_COOLDOWN = 80;
volatile long last_button_press_t;


volatile int rising_count = 0;
volatile long t = 0;
volatile long prev_t = 0;
volatile long prev_prev_t = 0;
void countingRising(){
  rising_count += 1;
  prev_prev_t = prev_t;
  prev_t = t,
  t = micros();
}           

void buttonPressed()                                              //
{           
          
  long current_millis = millis();
  delay(10);  //so when we digitalRead() we can be sure what's up
  if ( current_millis - last_button_press_t > PRESS_REGISTERING_COOLDOWN){
    /*if (digitalRead(PB8) == HIGH){
    i += 100;
    } else if (digitalRead(PB8) == LOW) {
      if(ledOn)                                                       //  if statement depends on LedOn value
      {
        ledOn=false;                                                 //  Makes ledOn false if it is True
        digitalWrite(LED_BUILTIN,LOW);                                       //  digital writs the low vale to PA1 pin makes led OFF
      }
      else
      {
        ledOn = true;                                                //  Makes ledOn True if it is False
        digitalWrite(LED_BUILTIN,HIGH);                                      //  digital writs the HIGH vale to PA1 pin makes led ON
      }
    }*/
    last_button_press_t = current_millis;   
  }
}

void show_display(){ 
  int y=20;
  noInterrupts();
  int number = button_state;
  int n_2 = rising_count;
  long t3 = t;
  long t2 = prev_t;
  long t1 = prev_prev_t;
  interrupts();
  u8g2.clearBuffer();         // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(5*number, 14, u8x8_utoa(number));
  u8g2.drawStr(50, 14, u8x8_utoa(state));
  u8g2.sendBuffer();
}

void t1Callback() {
  show_display();
}

void t2Callback() {
  read_button(PB8, last_button_press_t, button_state);
  updateSate();
}

void updateSate(){
  if (button_state == JUST_LEFT_SHORT_PRESS) state += 1;
  if (button_state == JUST_ENTERED_LONG_PRESS) state += 10;
}

int read_button(int BUTTON_PIN,volatile long &button_press_start, int &button_state){
  long current_millis = millis();
  bool button_pressed = false;
  if (digitalRead(BUTTON_PIN) == LOW) button_pressed = true;
  /*

    We want to act when we leave shortpress and when we enter longpress
    needed states:
    
  */
  switch (button_state){
    case IDLE:
      if (button_pressed) {
        button_state = SHORT_PRESS;
        //button_press_start = current_millis;
      }
      break;
    case SHORT_PRESS:
      if (button_pressed) {
        if (current_millis - button_press_start > LONG_PRESS_TRESHOLD) button_state = JUST_ENTERED_LONG_PRESS;
      } else {
        /*if (current_millis - button_press_start > SHORT_PRESS_TRESHOLD)*/ button_state = JUST_LEFT_SHORT_PRESS;
      }
      break;
    case JUST_ENTERED_LONG_PRESS:
      if (button_pressed) {
        button_state = LONG_PRESS;
      } else {
        button_state = IDLE;
      }
      break;
    case JUST_LEFT_SHORT_PRESS:
      if (button_pressed) {
        button_state = SHORT_PRESS;
      } else {
        button_state = IDLE;
      }
      break;
    case LONG_PRESS:
      if (button_pressed) {
      } else {
        button_state = IDLE;
      }
      break;
  }
}