#include <Arduino.h>
#include <U8g2lib.h>
//#include <Wire.h>

//U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE);

int RPM_SCREEN = 1;
int MICROSTEP_SETTING_SCREEN = 2;
int MAX_SETTING_SCREEN = 3;
int MIN_SETTING_SCREEN = 4;
//int _SETTING_SCREEN = 5;
int screen = RPM_SCREEN;
int number = 1;
int start_x = 16;
int start_y = 62;
int delta = 32;
int RPM_TEXT_X = 77;//82;
void show_display(){
  u8g2.clearBuffer();					// clear the internal memory
  if (screen == RPM_SCREEN){  
    draw_number_screen("RPM", RPM_TEXT_X-start_x);
  } else if(screen == MICROSTEP_SETTING_SCREEN ){
    draw_number_screen("MICROSTEP", 0);
  } else if(screen == MAX_SETTING_SCREEN){
    draw_number_screen("MAX RPM", 0);
  } else if(screen == MIN_SETTING_SCREEN){
    draw_number_screen("MIN RPM", 0);
  }
  u8g2.sendBuffer();
}

void draw_number_screen(char * heading, int addition){
  u8g2.setFont(u8g2_font_7Segments_26x42_mn);
    if (number > 99){
      u8g2.drawStr(start_x, start_y, u8x8_utoa(number));
    } else if (number > 9){
      u8g2.drawStr(start_x + delta, start_y, u8x8_utoa(number));
    } else {
      u8g2.drawStr(start_x + delta + delta, start_y, u8x8_utoa(number));  
    }
    u8g2.setFont(u8g2_font_logisoso16_tf);//u8g2_font_freedoomr10_mu  );
    u8g2.drawStr(start_x + addition, 16, heading);	// write something to the internal memory
     /*
      u8g2_font_logisoso16_tf
      u8g2_font_freedoomr10_mu
    */
   const int segment = ((millis() % 1000)/250)%4;
    switch (segment)
    {
    case 0:
        u8g2.drawCircle(4 , 4, 4, U8G2_DRAW_UPPER_RIGHT);    
        break;
    case 1:
        u8g2.drawCircle(4 , 4, 4, U8G2_DRAW_UPPER_LEFT);
        break;
    case 2:
        u8g2.drawCircle(4 , 4, 4, U8G2_DRAW_LOWER_LEFT);
        break;
    case 3:
        u8g2.drawCircle(4 , 4, 4, U8G2_DRAW_LOWER_RIGHT);
        break;
    default:
        break;
    }
}

void setup(void) {
  u8g2.begin();
}

void loop(void) {
  screen += 1;
  if (screen == 5) screen = 1;
  number += 3;
  if (number>300) number = 1;
  show_display();
  delay(200);  
}