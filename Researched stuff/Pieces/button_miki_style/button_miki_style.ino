//INTERRUPTS IN STM32F103C8
//CIRCUIT DIGEST
#include <Arduino.h>
#include <U8g2lib.h>
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
#include <TaskScheduler.h>

void t1Callback();
void t2Callback();

Task t1(1000, TASK_FOREVER, &t1Callback);
Task t2(1000/1000, TASK_FOREVER, &t2Callback);
Scheduler runner;

const int LONG_PRESS_TRESHOLD = 1000;

int button_state;
int state = 0;

void setup(){
  u8g2.begin();     
  pinMode(PB8,INPUT_PULLUP);          
  pinMode(PB9,INPUT_PULLUP);      
  /*attachInterrupt(digitalPinToInterrupt(PB8),buttonPressed,FALLING);                      //  function for creating external interrupts
  attachInterrupt(digitalPinToInterrupt(PB9),countingRising,RISING);*/
  runner.init();
  runner.addTask(t1);
  runner.addTask(t2);
  delay(1000);
  t1.enable();
  t2.enable();
}
          
const int NOTHING_PRESSED = 0;
const int PRESSED_AND_RELEASED = 1;
const int HOLDING_STARTS = 2;
const int HELD = 3;
bool ignore_next_button_release = true;
const int BUTTON_HOLD_DURATION_MINIMUM = 1000;
int button_hold_counts_1 = 0;
int button_m = 0;

void show_display(){ 
  int y=20;
  noInterrupts();
  int number = button_m;
  int n_2 = button_hold_counts_1;
  if (button_state == HELD){
    u8g2.drawStr(50, 50, "aaaa");
  }
  /*int n_2 = rising_count;
  long t3 = t;
  long t2 = prev_t;
  long t1 = prev_prev_t;*/
  interrupts();
  u8g2.clearBuffer();         // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.drawStr(5, 14, u8x8_utoa(number));
  u8g2.drawStr(5, 50, u8x8_utoa(n_2));
  u8g2.drawStr(50, 14, u8x8_utoa(state));
  u8g2.sendBuffer();
}

void t1Callback() {
  //show_display();
}



bool disp_needed = true;
void t2Callback() {
  updateButtonSate();
  updateSate();
  if (disp_needed) {
    show_display();
    disp_needed = false;
  }
}

void updateButtonSate(){
  bool button_1_released = false;
  if (digitalRead(PB8) == LOW) button_hold_counts_1 += 1;
  else if (button_hold_counts_1 > 0) {
    button_1_released = true;
    button_m = button_hold_counts_1;
    button_hold_counts_1 = 0;
  }
  if (button_hold_counts_1 == BUTTON_HOLD_DURATION_MINIMUM) {
    button_state = HOLDING_STARTS;
    ignore_next_button_release = true;
  } else if (button_hold_counts_1 > BUTTON_HOLD_DURATION_MINIMUM){
    button_state = HELD;
    ignore_next_button_release = true;
  } else if (button_1_released && !ignore_next_button_release) {
    button_state = PRESSED_AND_RELEASED;
  } else if (button_1_released && ignore_next_button_release) {
    button_state = NOTHING_PRESSED;
    ignore_next_button_release = false;
  } else {
    button_state = NOTHING_PRESSED;
  }
}

void updateSate(){
  switch (button_state){
    case PRESSED_AND_RELEASED:
      state += 1;
      ///button_state = NOTHING_PRESSED;
      disp_needed = true;
      break;
    case HOLDING_STARTS:
      state += 10;
      disp_needed = true;
      //button_state = NOTHING_PRESSED;
      break;
    case HELD:
      //state = 0;
      //button_state = NOTHING_PRESSED;
      break;
  }
}




void loop()                                                       //  void loops runs continuously
{  
  runner.execute();                                              //  delays time 
}