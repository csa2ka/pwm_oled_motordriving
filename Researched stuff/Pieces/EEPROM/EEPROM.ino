#include <EEPROM.h>

#include <Arduino.h>
#include <U8g2lib.h>
//#include <Wire.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

int RPM_SCREEN = 1;
int MICROSTEP_SETTING_SCREEN = 2;
int MAX_SETTING_SCREEN = 3;
int MIN_SETTING_SCREEN = 4;
//int _SETTING_SCREEN = 5;
int screen = RPM_SCREEN;
int number = 1;
int start_x = 16;
int start_y = 62;
int delta = 32;
int RPM_TEXT_X = 77;//82;

const int addressEEPROM_min = 0;              // Specify the address restrictions you want to use.
const int addressEEPROM_max = 4095;           // For example, the maximum is 4095, which means we allocate 4KB of memory (4096 bytes) for the Virtual EEPROM.

float latitude =   4.158919;                  // This is an example of data that you want to save to EEPROM.
float longitude = 96.124843;                  // Coordinate data consisting of Latitude and Longitude.

int addressLat = 0;                           // The number of characters in the latitude value is 8, so the address starts from 0 to 8.
int addressLon = 9;                           // The number of characters in the longitude value is 9, so the address starts from 9 to 17.

String lat = String (latitude, 6);         // Data to be saved to EEPROM is a String. This line is used for the conversion of float to String.
String lon = String (longitude, 6);        // Value 6 shows how many digits behind the comma will be converted.

void show_display(){
  u8g2.clearBuffer();					// clear the internal memory
  if (screen == RPM_SCREEN){  
    draw_number_screen("RPM", RPM_TEXT_X-start_x);
  } else if(screen == MICROSTEP_SETTING_SCREEN ){
    draw_number_screen("MICROSTEP", 0);
  } else if(screen == MAX_SETTING_SCREEN){
    draw_number_screen("MAX RPM", 0);
  } else if(screen == MIN_SETTING_SCREEN){
    draw_number_screen("MIN RPM", 0);
  }
  u8g2.sendBuffer();
}

void draw_number_screen(char * heading, int addition){
  u8g2.setFont(u8g2_font_7Segments_26x42_mn);
    if (number > 99){
      u8g2.drawStr(start_x, start_y, u8x8_utoa(number)  );
    } else if (number > 9){
      u8g2.drawStr(start_x + delta, start_y, u8x8_utoa(number));
    } else {
      u8g2.drawStr(start_x + delta + delta, start_y, u8x8_utoa(number));  
    }
    const char * c_lat = lat.c_str(); 
    //u8g2.drawStr(start_x, start_y, c_lat  );
    u8g2.setFont(u8g2_font_logisoso16_tf);//u8g2_font_freedoomr10_mu  );
    u8g2.drawStr(1, 16, c_lat);	// write something to the internal memory
     /*
      u8g2_font_logisoso16_tf
      u8g2_font_freedoomr10_mu
    */
}

void setup()
{
  u8g2.begin();
  /*Serial.begin(9600);
  delay(100);*/
  // ------------------- Write Data Commands -------------------
  /*Serial.println(" -----------------------------------------");
  Serial.println("| Writing Latitude to EEPROM  | Saved |");*/
  //EEPROMwrite(addressLat, lat);
  //Serial.println("| Writing Longitude to EEPROM | Saved |");
  //EEPROMwrite(addressLon, lon);
  /*Serial.println(" -----------------------------------------");
  Serial.println("\n");*/
  delay(1000);
  //----------------------------------------------------------

  // ------------------- Read Data Commands -------------------
  /*Serial.println("Baca data dari EEPROM.....");
  Serial.print("Latitude  : ");*/
  lat = EEPROMread(addressLat, 8);  // Get data from address 0 with 8 characters.
  /*Serial.print("Longitude : ");*/
  lon = EEPROMread(addressLon, 9);  // Get data from address 9 with a total of 9 characters.
  /*Serial.println("\n");*/
  delay(1000);
  //----------------------------------------------------------

  //Serial.println("Now, please COMMENT *Write Data Commands* to deactivate data write commands to EEPROM.");
  delay(5000);
  //Serial.println("Then, please press the reset button or remove STM32 from the computer to see whether the data from the EEPROM is still stored or not.");
} 

void loop()
{
   show_display();
   delay(10);
}

String EEPROMread(int StartAddr, int StringLength)
{
  char buf[StringLength + 1];
  eeprom_read_string(StartAddr, buf, StringLength + 1);
  String dataStr = buf;
  return dataStr;
}

void EEPROMwrite(int StartAddr, String DataString)
{
  int val = DataString.length() + 1;
  char StringChar[val];
  char buff[val];

  DataString.toCharArray(StringChar, val);
  strcpy(buff, StringChar);
  eeprom_write_string(StartAddr, buff);
}

boolean eeprom_is_addr_ok(int addr)
{
  return ((addr >= addressEEPROM_min) && (addr <= addressEEPROM_max));
}

boolean eeprom_write_bytes(int startAddr, const byte* array, int numBytes)
{
  int i;

  if (!eeprom_is_addr_ok(startAddr) || !eeprom_is_addr_ok(startAddr + numBytes))
  {
    return false;
  }

  for (i = 0; i < numBytes; i++)
  {
    EEPROM.write(startAddr + i, array[i]);
  }
  return true;
}

boolean eeprom_write_string(int addr, const char* string)
{
  int numBytes;                                                    // number of actual bytes to be written

  numBytes = strlen(string) + 1;                                   // Write string content plus byte terminator string (0x00)
  return eeprom_write_bytes(addr, (const byte*)string, numBytes);
}

boolean eeprom_read_string(int addr, char* buffer, int bufSize)
{
  byte ch;                                                         // read bytes from eeprom
  int bytesRead;                                                   // number of bytes read so far

  if (!eeprom_is_addr_ok(addr))                                    // check the starting address
  {
    return false;
  }

  if (bufSize == 0)                                                // how can we store bytes in an empty buffer?
  {
    return false;
  }

  if (bufSize == 1)                                                
  {
    buffer[0] = 0;
    return true;
  }

  bytesRead = 0;                                                   // initialize a byte counter
  ch = EEPROM.read(addr + bytesRead);                              // read the next byte from eeprom
  buffer[bytesRead] = ch;                                          // save it in the user's buffer
  bytesRead++;

  // if no stop conditions are met, read the next byte from the eeprom
  while ( (ch != 0x00) && (bytesRead < bufSize) && ((addr + bytesRead) <= addressEEPROM_max) )
  {
    ch = EEPROM.read(addr + bytesRead);
    buffer[bytesRead] = ch;                                        // save it in the user's buffer
    bytesRead++;
  }

  if ((ch != 0x00) && (bytesRead >= 1))                            // make sure the user buffer has the string terminator, (0x00) as the last byte
  {
    buffer[bytesRead - 1] = 0;
  }
  return true;
}
