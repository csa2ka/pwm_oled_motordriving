#define LED_RATE 500    // in microseconds; should give 0.5Hz toggles

#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock= SCL*/ PA5, /* data= SDA */ PA7, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display


// We'll use timer 2
HardwareTimer timer(2);
int low = 0;
int percent = 10;
void show_display(){
  u8g2.clearBuffer();          // clear the internal memory
  u8g2.setFont(u8g2_font_logisoso16_tf);//u8g2_font_freedoomr10_mu  );
  u8g2.drawStr(0, 16, u8x8_utoa(low));  // write something to the internal memory
  u8g2.drawStr(0, 36, u8x8_utoa(percent));  
  u8g2.sendBuffer();
}

void setup() {
  
  u8g2.begin();
  show_display();
    // Set up the LED to blink
    pinMode(LED_BUILTIN, OUTPUT);

    // Pause the timer while we're configuring it
    timer.pause();

    // Set up period
    timer.setPeriod(LED_RATE); // in microseconds

    // Set up an interrupt on channel 1
    timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
    timer.setCompare(TIMER_CH1, 1);  // Interrupt 1 count after each update
    timer.attachCompare1Interrupt(handler_led);

    // Refresh the timer's count, prescale, and overflow
    timer.refresh();

    // Start the timer counting
    timer.resume();
}


void loop() {
    // Nothing! It's all in the handler_led() interrupt:
    show_display();
    millis();
}


void toggleLED(){
  if (low%percent==0) digitalWrite(LED_BUILTIN, LOW);
  else digitalWrite(LED_BUILTIN, HIGH);
  low++;
  if (low%10==0){
    percent++; percent = percent % 100;  
  }
  
}

void handler_led(void) {
    toggleLED();
}
