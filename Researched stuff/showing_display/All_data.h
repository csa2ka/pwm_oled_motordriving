#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED
/* ^^ these are the include guards */

/* Prototypes for the functions */
/* Sums two ints */

typedef struct all_data {
    int an_int;
} all_data; 

all_data makeAll_data(int i);

void increase(struct all_data *ali);

void decrease(struct all_data *ali);


#endif