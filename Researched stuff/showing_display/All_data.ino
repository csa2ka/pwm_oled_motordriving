#include "All_data.h"
#include <stdbool.h>

all_data makeAll_data(int i){
    all_data ali;
    ali.an_int = i;
    return ali;
}

void increase(struct all_data *ali){
    ali->an_int ++;
}

void decrease(struct all_data *ali){
    ali->an_int --;
}
