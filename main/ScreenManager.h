#ifndef SCREENMANAGER_H_INCLUDED
#define SCREENMANAGER_H_INCLUDED
#include "Enums.h"
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, PA5, PA7, U8X8_PIN_NONE);
//U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, PB6, PB7, U8X8_PIN_NONE);
//U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE); //use PB6, and PB7

class ScreenManager
{
public:
    Screen current_screen;
    Screen current_setting_screen;
    int current_display_number;
    bool dir;
    int start_x;
    int start_y;
    int delta;
    int RPM_TEXT_X;
    bool motor_is_on;
    int saved_a_speed;
    int motor_speed;

public:
    ScreenManager();
    void ShowCurrentScreen();
    void DrawNumberScreen(char *heading, int addition, int number, bool is_main_screen, bool dir);
    void DrawSettingsScreen(char *heading);
    void circle_moving(int x, int y, int r, bool dir);
    void update(Screen current_screen_, Screen current_setting_screen_, 
                            int current_display_number_, bool dir_, bool motor_is_on);
};

#endif
