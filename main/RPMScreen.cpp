#include "StateMachine.h"
#include "Enums.h"
#include <EEPROM.h>

void RPMScreen::HandleEncoderUp()
{
    this->context_->setCurrentRPM(this->context_->current_rpm + 1);
    this->context_->number = this->context_->current_rpm;
}

void RPMScreen::HandleEncoderDown()
{
    this->context_->setCurrentRPM(this->context_->current_rpm - 1);
    this->context_->number = this->context_->current_rpm;
}

void RPMScreen::HandleButtonPress(ButtonEnum whichone, bool isshort)
{
    switch (whichone)
    {
    case BUTTON1:
        if (isshort)
        {
            this->context_->target_motor_power_is_on = !this->context_->target_motor_power_is_on;
        }
        else
        {
            this->context_->target_dir += 1;
            this->context_->target_dir %= 2;
        }
        break;
    case BUTTON2:
        if (!isshort)
        {
            this->context_->EEPROMSave(0, this->context_->current_rpm);
        }
        else
        {
            this->context_->setCurrentRPM(this->context_->EEPROMRead(0));
        }
        this->context_->number = this->context_->current_rpm;
        break;
    case BUTTON3:
        if (!isshort)
        {
            this->context_->EEPROMSave(1, this->context_->current_rpm);
        }
        else
        {
            this->context_->setCurrentRPM(this->context_->EEPROMRead(1));
        }
        this->context_->number = this->context_->current_rpm;
        break;
    case BUTTON4:
        if (!isshort)
        {
            this->context_->EEPROMSave(2, this->context_->current_rpm);
        }
        else
        {
            this->context_->setCurrentRPM(this->context_->EEPROMRead(2));
        }
        this->context_->number = this->context_->current_rpm;
        break;
    case BUTTON_ENCODER:
        if (!isshort)
        {
            this->context_->target_motor_power_is_on = false;
            this->context_->current_screen = SETTINGS_SCREEN;
            this->context_->TransitionTo(new SettingScreen);
        }
        break;
    default:
        break;
    }
}
