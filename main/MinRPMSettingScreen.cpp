#include "StateMachine.h"

void MinRPMSettingScreen::HandleEncoderUp()
{
    this->context_->min_rpm++;
    if (this->context_->min_rpm > this->context_->max_rpm)
    {
        this->context_->min_rpm = this->context_->max_rpm;
    }
    this->context_->number = this->context_->min_rpm;
    this->context_->setCurrentRPM(this->context_->current_rpm);
    this->context_->EEPROMSave(6, this->context_->min_rpm);
}

void MinRPMSettingScreen::HandleEncoderDown()
{
    this->context_->min_rpm--;
    if (this->context_->min_rpm < 0)
    {
        this->context_->min_rpm = 0;
    }
    this->context_->number = this->context_->min_rpm;
    this->context_->setCurrentRPM(this->context_->current_rpm);
    this->context_->EEPROMSave(6, this->context_->min_rpm);
}

void MinRPMSettingScreen::HandleButtonPress(ButtonEnum whichone, bool isshort)
{
    if (whichone == BUTTON_ENCODER)
    {
        if (!isshort)
        {
            this->context_->number = this->context_->current_rpm;
            this->context_->current_screen = RPM_SCREEN;
            this->context_->TransitionTo(new RPMScreen);
        }
        else
        {
            this->context_->current_screen = SETTINGS_SCREEN;
            this->context_->TransitionTo(new SettingScreen);
        }
    }
}