#include "StateMachine.h"

void SettingScreen::HandleEncoderUp()
{
    //this->context_->TransitionTo(new SettingScreen);
    switch (this->context_->current_setting_screen)
    {
    case MAX_SETTING_SCREEN:
        this->context_->current_setting_screen = MIN_SETTING_SCREEN;
        break;
    case MIN_SETTING_SCREEN:
        this->context_->current_setting_screen = MICROSTEP_SETTING_SCREEN;
        break;
    case MICROSTEP_SETTING_SCREEN:
        this->context_->current_setting_screen = MAX_SETTING_SCREEN;
        break;
    default:
        break;
    }
}

void SettingScreen::HandleEncoderDown()
{
    switch (this->context_->current_setting_screen)
    {
    case MAX_SETTING_SCREEN:
        this->context_->current_setting_screen = MICROSTEP_SETTING_SCREEN;
        break;
    case MIN_SETTING_SCREEN:
        this->context_->current_setting_screen = MAX_SETTING_SCREEN;
        break;
    case MICROSTEP_SETTING_SCREEN:
        this->context_->current_setting_screen = MIN_SETTING_SCREEN;
        break;
    default:
        break;
    }
}

void SettingScreen::HandleButtonPress(ButtonEnum whichone, bool isshort)
{
    if (whichone == BUTTON_ENCODER)
    {
        if (isshort)
        {
            switch (this->context_->current_setting_screen)
            {
            case MICROSTEP_SETTING_SCREEN:
                this->context_->current_screen = MICROSTEP_SETTING_SCREEN;
                this->context_->number = this->context_->microstep;
                this->context_->TransitionTo(new MicrostepSettingScreen);
                break;
            case MAX_SETTING_SCREEN:
                this->context_->current_screen = MAX_SETTING_SCREEN;
                this->context_->number = this->context_->max_rpm;
                this->context_->TransitionTo(new MaxRPMSettingScreen);
                break;
            case MIN_SETTING_SCREEN:
                this->context_->current_screen = MIN_SETTING_SCREEN;
                this->context_->number = this->context_->min_rpm;
                this->context_->TransitionTo(new MinRPMSettingScreen);
                break;
            default:
                break;
            }
        }
        else
        {
            this->context_->number = this->context_->current_rpm;
            this->context_->current_screen = RPM_SCREEN;
            this->context_->TransitionTo(new RPMScreen);
        }
    }
}

/*MICROSTEP_SETTING_SCREEN,
    MAX_SETTING_SCREEN,
    MIN_SETTING_SCREEN,*/