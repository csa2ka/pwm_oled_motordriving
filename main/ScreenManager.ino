#include "ScreenManager.h"
#include <stdbool.h>
#include "Enums.h"

ScreenManager::ScreenManager()
{
    current_screen = RPM_SCREEN;
    current_setting_screen = MIN_SETTING_SCREEN;
    current_display_number = 0;
    dir = false;
    start_x = 16;
    start_y = 62;
    delta = 32;
    RPM_TEXT_X = 77; //82;
    motor_is_on = false;
    saved_a_speed = 0;
    motor_speed = 50;
}

void ScreenManager::ShowCurrentScreen()
{
    u8g2.clearBuffer();
    switch (current_screen)
    {
    case RPM_SCREEN:
        DrawNumberScreen("RPM", RPM_TEXT_X - start_x, current_display_number, true, dir);
        break;
    case MICROSTEP_SETTING_SCREEN:
        DrawNumberScreen("MICROSTEP", 0, current_display_number, false, false);
        break;
    case MAX_SETTING_SCREEN:
        DrawNumberScreen("MAX RPM", 0, current_display_number, false, false);
        break;
    case MIN_SETTING_SCREEN:
        DrawNumberScreen("MIN RPM", 0, current_display_number, false, false);
        break;
    case SETTINGS_SCREEN:
        switch (current_setting_screen)
        {
        case MAX_SETTING_SCREEN:
            DrawSettingsScreen("MAX");
            break;
        case MIN_SETTING_SCREEN:
            DrawSettingsScreen("MIN");
            break;
        case MICROSTEP_SETTING_SCREEN:
            DrawSettingsScreen("MICROSTEP");
            break;
        default:
            break;
        }
    default:
        break;
    }
    u8g2.sendBuffer();
}

void ScreenManager::circle_moving(int x, int y, int r, bool dir)
{
    int period_time = (60000 / motor_speed); //10-50   one_round_s_time = 1 / round_per_minute
    int segment = ((millis() % period_time) / (period_time / 4)) % 4;
    if (!dir)
        segment = (8 - segment) % 4;
    switch (segment)
    {
    case 0:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_UPPER_RIGHT);
        break;
    case 1:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_UPPER_LEFT);
        break;
    case 2:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_LOWER_LEFT);
        break;
    case 3:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_LOWER_RIGHT);
        break;
    default:
        break;
    }
}

void ScreenManager::DrawNumberScreen(char *heading, int addition, int number, bool is_main_screen = false, bool dir = false)
{
    u8g2.clearBuffer();

    u8g2.setFont(u8g2_font_7Segments_26x42_mn);
    if (number > 99)
    {
        u8g2.drawStr(start_x, start_y, u8x8_utoa(number));
    }
    else if (number > 9)
    {
        u8g2.drawStr(start_x + delta, start_y, u8x8_utoa(number));
    }
    else
    {
        u8g2.drawStr(start_x + delta + delta, start_y, u8x8_utoa(number));
    }
    u8g2.setFont(u8g2_font_logisoso16_tf); //u8g2_font_freedoomr10_mu  );
    if (is_main_screen)
    {
        //u8g2.drawStr(start_x, 16, u8x8_utoa(111));
        if (!dir)
            u8g2.drawTriangle(1, 38, 9, 38, 5, 30);
        else
            u8g2.drawTriangle(1, 42, 9, 42, 5, 50);
        if (motor_is_on)
        {
            circle_moving(6, 6, 6, dir);
        }
        if (saved_a_speed > 0)
        {
            u8g2.drawStr(18, 16, "Saved");
            saved_a_speed--;
        }
    }                                              // write something to the internal memory
    u8g2.drawStr(start_x + addition, 16, heading); // write something to the internal memory

    u8g2.sendBuffer();
}

void ScreenManager::DrawSettingsScreen(char *heading)
{
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_logisoso16_tf);
    u8g2.drawStr(15, 16, heading);
    u8g2.sendBuffer();
}

void ScreenManager::update(Screen current_screen_, Screen current_setting_screen_,
                                        int current_display_number_, bool dir_, bool motor_is_on_)
{
    current_screen = current_screen_;
    current_setting_screen = current_setting_screen_;
    current_display_number = current_display_number_;
    dir = dir_;
    motor_is_on = motor_is_on_;
}