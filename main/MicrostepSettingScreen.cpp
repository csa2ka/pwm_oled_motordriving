#include "StateMachine.h"

void MicrostepSettingScreen::HandleEncoderUp()
{
    //this->context_->TransitionTo(new SettingScreen);
    this->context_->microstep *= 2;
    if (this->context_->microstep > this->context_->MAX_MICROSTEP)
    {
        this->context_->microstep = this->context_->MAX_MICROSTEP;
    }
    this->context_->number = this->context_->microstep;
    this->context_->EEPROMSave(7, this->context_->microstep);
}

void MicrostepSettingScreen::HandleEncoderDown()
{
    this->context_->microstep /= 2;
    if (this->context_->microstep < this->context_->MIN_MICROSTEP)
    {
        this->context_->microstep = this->context_->MIN_MICROSTEP;
    }
    this->context_->number = this->context_->microstep;
    this->context_->EEPROMSave(7, this->context_->microstep);
}

void MicrostepSettingScreen::HandleButtonPress(ButtonEnum whichone, bool isshort)
{
    if (whichone == BUTTON_ENCODER)
    {
        if (!isshort)
        {
            this->context_->number = this->context_->current_rpm;
            this->context_->current_screen = RPM_SCREEN;
            this->context_->TransitionTo(new RPMScreen);
        }
        else
        {
            this->context_->current_screen = SETTINGS_SCREEN;
            this->context_->TransitionTo(new SettingScreen);
        }
    }
}