#ifndef ENCODER_H_INCLUDED
#define ENCODER_H_INCLUDED
/* ^^ these are the include guards */

/* Prototypes for the functions */
/* Sums two ints */

#define ENCODER_INPUT_A PB13
#define ENCODER_INPUT_B PB14

const int NOTHING_ENCODER = 0;
const int UP_ENCODER = 1;
const int DOWN_ENCODER = 2;

class Encoder
{
public:
    int counter;
    int upper_bound;
    int lower_bound;
    int aState;
    int aLastState;
    bool data_changed;
    bool showing_disp;
    long prev_t;
    long prev_read_time;
    long last_time_display_was_shown;
    long display_waiting;

public:
    Encoder();

    int read();
    bool request_displaying();
    void reset_before_display();
    void reset_after_display();
    int handle_rotation(int increment);
};

#endif
