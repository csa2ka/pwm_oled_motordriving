#include "StateMachine.h"
#include <Arduino.h>
#include <U8g2lib.h>
#include "Encoder.h"
#include "ScreenManager.h"
#include "Button.h"
#include "MotorHandling.h"
#include <EEPROM.h>

#define BUTTON1_PIN PB6
#define BUTTON2_PIN PB9
#define BUTTON3_PIN PB7
#define BUTTON4_PIN PB8
#define PWM_PIN PA6
#define DIRECTION_PIN PB1
#define ENCODER_B_PIN PB13
#define ENCODER_A_PIN PB14
#define ENCODER_BUTTON_PIN PB12

Encoder the_encoder;
ScreenManager sc_manager;
Button the_encoder_button(ENCODER_BUTTON_PIN, BUTTON_ENCODER);
Button button1(BUTTON1_PIN, BUTTON1);
Button button2(BUTTON2_PIN, BUTTON2);
Button button3(BUTTON3_PIN, BUTTON3);
Button button4(BUTTON4_PIN, BUTTON4);
StateMachine *stateMachine = new StateMachine(new RPMScreen);
MotorHandler motorHandler(DIRECTION_PIN, PWM_PIN);
HardwareTimer timer(2);   //maybe a timerHandler? it could elmininate code from here

void handle_encoder_input(int encoder_input)
{
  switch (encoder_input)
  {
  case NOTHING_ENCODER:
    break;
  case UP_ENCODER:
    stateMachine->EncoderUp();
    break;
  case DOWN_ENCODER:
    stateMachine->EncoderDown();
    break;
  }
}

bool button_changed_something = false;
void handle_button_input(ButtonState button_input, ButtonEnum btnenum)
{
  switch (button_input)
  {
  case PRESSED_AND_RELEASED:
    stateMachine->ButtonPress(btnenum, true);
    button_changed_something = true;
    break;
  case HOLDING_STARTS:
    stateMachine->ButtonPress(btnenum, false);
    button_changed_something = true;
    if (btnenum == BUTTON2 || btnenum == BUTTON3 || btnenum == BUTTON4)
      sc_manager.saved_a_speed = 4;
    break;
  }
}

long timi = 0;
const long ACCELERATION = 750; //RPM/s
void read_input_and_update_pwmHandler()
{
  handle_encoder_input(the_encoder.read());
  handle_button_input(the_encoder_button.readState(), the_encoder_button.btnenum); //TODO use arrays if too many buttons
  handle_button_input(button1.readState(), button1.btnenum);
  handle_button_input(button2.readState(), button2.btnenum);
  handle_button_input(button3.readState(), button3.btnenum);
  handle_button_input(button4.readState(), button4.btnenum);
  timi++;
  if (timi > (1000000 / 1000) / ACCELERATION)         //TODO ocsmány
  { //TODO put this inside motorHandler.update()
    motorHandler.updateMotor();
    timi = 0;
  }
}

void setup_timer(int period_length)
{
  timer.pause();
  timer.setPeriod(period_length);
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);
  timer.attachCompare1Interrupt(read_input_and_update_pwmHandler);
  timer.refresh();
  timer.resume();
}

void get_data_from_EEPROM()
{
  stateMachine->target_dir = 1; //stateMachine->EEPROMRead(4);
  stateMachine->max_rpm = 1000; //stateMachine->EEPROMRead(5);
  stateMachine->min_rpm = 10; //stateMachine->EEPROMRead(6);
  stateMachine->microstep = 64; //stateMachine->EEPROMRead(7);

  stateMachine->setCurrentRPM(100); //stateMachine->EEPROMRead(3));
  stateMachine->number = stateMachine->current_rpm;
}

void save_current_rpm_to_eeprom()
{
  stateMachine->EEPROMSave(3, stateMachine->current_rpm);
  stateMachine->EEPROMSave(4, stateMachine->target_dir);
}

/*void show_display()
{
  /*sc_manager.motor_is_on = stateMachine->target_motor_power_is_on;
  sc_manager.motor_speed = stateMachine->current_rpm;
  if (stateMachine->current_screen == SETTINGS_SCREEN)
  {
    switch (stateMachine->current_setting_screen)
    {
    case MAX_SETTING_SCREEN:
      sc_manager.DrawSettingsScreen("MAX");
      break;
    case MIN_SETTING_SCREEN:
      sc_manager.DrawSettingsScreen("MIN");
      break;
    case MICROSTEP_SETTING_SCREEN:
      sc_manager.DrawSettingsScreen("MICROSTEP");
      break;
    default:
      break;
    }
  }
  else
  {
    sc_manager.ShowCurrentScreen(stateMachine->current_screen, stateMachine->number, stateMachine->target_dir);
  }/*
  sc_manager.update(stateMachine->current_screen, stateMachine->current_setting_screen, 
                                stateMachine->number, stateMachine->target_dir, stateMachine->target_motor_power_is_on);
  sc_manager.ShowCurrentScreen();                             
}*/

//=============================================================================================================================

void setup()
{
  get_data_from_EEPROM();
  //motorHandler.current_dir = stateMachine->target_dir;
  u8g2.begin();
  setup_timer(1000);
}

void loop() //loop runs as it pleases: it determined by the length of displaying the screen, it's around 20-25 FPS at this time
{
  /*if (the_encoder.request_displaying() || button_changed_something)
  {*/
  motorHandler.updateValues(stateMachine->current_rpm, stateMachine->target_dir,
                            stateMachine->target_motor_power_is_on, stateMachine->max_rpm,
                            stateMachine->min_rpm, stateMachine->microstep);
  save_current_rpm_to_eeprom();
  //}
  //show_display();
  sc_manager.update(stateMachine->current_screen, stateMachine->current_setting_screen, 
                    stateMachine->number, stateMachine->target_dir, stateMachine->target_motor_power_is_on);
  sc_manager.ShowCurrentScreen();   
}
