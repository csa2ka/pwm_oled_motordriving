
#ifndef STATEMACHINE_H
#define STATEMACHINE_H
#include "Enums.h"
#include <EEPROM.h>
//#include "MaxRPMSettingScreen.h"

class StateMachine;

class State
{
    /**
   * @var Context
   */
protected:
    StateMachine *context_;

public:
    virtual ~State() {}

    void set_context(StateMachine *context) { this->context_ = context; }
    virtual void HandleEncoderUp() = 0;
    virtual void HandleEncoderDown() = 0;
    virtual void HandleButtonPress(ButtonEnum whichone, bool isshort) = 0;
};

/**
 * The Context defines the interface of interest to clients. It also maintains a
 * reference to an instance of a State subclass, which represents the current
 * state of the Context.
 */

class StateMachine
{
    /**
   * @var State A reference to the current state of the Context.
   */
private:
    State *state_;

public:
    Screen current_screen;
    int current_rpm;
    int max_rpm;
    int min_rpm;
    int microstep;
    int number;
    short target_dir;
    bool target_motor_power_is_on;
    Screen current_setting_screen;
    const int NUMBER_OF_SETTING_SCREENS = 3;
    const int MAX_MICROSTEP = 128;
    const int MIN_MICROSTEP = 32;

    //StateMachine(State *state);           //TODO
    StateMachine(State *state) : state_(nullptr)
    {
        target_dir = 0;
        target_motor_power_is_on = false;
        current_rpm = 20;
        max_rpm = 360;
        min_rpm = 10;
        microstep = 128;
        number = current_rpm;
        current_setting_screen = MIN_SETTING_SCREEN;
        current_screen = RPM_SCREEN;

        this->TransitionTo(state);
    }
    ~StateMachine()
    {
        delete state_;
    }
    void EEPROMSave(int address, int number);
    int EEPROMRead(int address);
    void TransitionTo(State *state);
    void EncoderUp();
    void EncoderDown();
    void ButtonPress(ButtonEnum whichone, bool isshort);
    void setCurrentRPM(int rpm);
};

class RPMScreen : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
};

class SettingScreen : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
};

class MaxRPMSettingScreen : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
};

class MinRPMSettingScreen : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
};

class MicrostepSettingScreen : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
};

#endif
