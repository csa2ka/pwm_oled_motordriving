#ifndef MOTORHANDLING_H
#define MOTORHANDLING_H

HardwareTimer pwmtimer(3);

class MotorHandler
{
private:
public:
    short microstep = 128;          //Should be between these: 1, 2, 4, 8, ... , 128
    const long CPU_FREQ = 72000000; //Hertz
    const short STEP_PER_REVOLUTION = 200;
    short max_rpm = 300;
    short min_rpm = 20;
    short target_rpm = 20;
    short current_rpm = 0;
    short target_dir = 1;
    short current_dir = 0;
    bool target_is_on = false;
    bool current_is_on = false;

    short dirpin;
    short pwm_pin;
    MotorHandler(short dirpinn, short steppinn);
    void PWM_turn_off();
    void PWM_turn_on();
    void change_direction();
    bool set_frequency();
    //void set_microstep(short mic);
    bool set_rpm(short target_rpm_);
    bool set_frequency(double target_freq_);
    void updateValues(short target_rpm, short target_dirr, bool target_is_onn, short max_rpm, short min_rpm, short microstep);
    void updateMotor();
};

#endif
