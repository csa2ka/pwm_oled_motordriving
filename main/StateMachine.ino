
/**
 * The base State class declares methods that all Concrete State should
 * implement and also provides a backreference to the StateMachine object, associated
 * with the State. This backreference can be used by States to transition the
 * StateMachine to another State.
 */
#include "StateMachine.h"

void StateMachine::TransitionTo(State *state)
{
    if (this->state_ != nullptr)
        delete this->state_;
    this->state_ = state;
    this->state_->set_context(this);
}

void StateMachine::EEPROMSave(int address, int number)
{
    int first_half = 0;
    int second_half = 0;
    if (number > 255)
    {
        first_half = 255;
        number -= 255;
        second_half = number;
    }
    else
    {
        first_half = number;
    }
    EEPROM.write(address * 2, first_half);
    EEPROM.write(address * 2 + 1, second_half);
}

int StateMachine::EEPROMRead(int address)
{
    return EEPROM.read(address * 2) + EEPROM.read(address * 2 + 1);
}

void StateMachine::EncoderUp()
{
    this->state_->HandleEncoderUp();
}

void StateMachine::setCurrentRPM(int rpm)
{
    current_rpm = rpm;
    if (current_rpm < min_rpm)
        current_rpm = min_rpm;
    if (current_rpm > max_rpm)
        current_rpm = max_rpm;
}

void StateMachine::EncoderDown()
{
    this->state_->HandleEncoderDown();
}

void StateMachine::ButtonPress(ButtonEnum btnenum, bool isshort)
{
    //this->number = 13;
    this->state_->HandleButtonPress(btnenum, isshort);
}