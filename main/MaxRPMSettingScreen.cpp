#include "StateMachine.h"

void MaxRPMSettingScreen::HandleEncoderUp()
{
    //this->context_->TransitionTo(new SettingScreen);
    this->context_->max_rpm++;
    this->context_->number = this->context_->max_rpm;
    this->context_->setCurrentRPM(this->context_->current_rpm);
    this->context_->EEPROMSave(5, this->context_->max_rpm);
}

void MaxRPMSettingScreen::HandleEncoderDown()
{
    this->context_->max_rpm--;
    if (this->context_->max_rpm < this->context_->min_rpm)
    {
        this->context_->max_rpm = this->context_->min_rpm;
    }
    this->context_->number = this->context_->max_rpm;
    this->context_->setCurrentRPM(this->context_->current_rpm);
    this->context_->EEPROMSave(5, this->context_->max_rpm);
}

void MaxRPMSettingScreen::HandleButtonPress(ButtonEnum whichone, bool isshort)
{
    if (whichone == BUTTON_ENCODER)
    {
        if (!isshort)
        {
            this->context_->number = this->context_->current_rpm;
            this->context_->current_screen = RPM_SCREEN;
            this->context_->TransitionTo(new RPMScreen);
        }
        else
        {
            this->context_->current_screen = SETTINGS_SCREEN;
            this->context_->TransitionTo(new SettingScreen);
        }
    }
}