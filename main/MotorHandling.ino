#include "MotorHandling.h"

MotorHandler::MotorHandler(short dirpinn, short steppinn)
{
    dirpin = dirpinn;
    pwm_pin = steppinn;
    pinMode(dirpin, OUTPUT);
}

void MotorHandler::PWM_turn_off()
{
    pinMode(pwm_pin, OUTPUT);
    current_is_on = false;
}

void MotorHandler::PWM_turn_on()
{
    pinMode(pwm_pin, PWM);
    current_is_on = true;
}

bool MotorHandler::set_rpm(short target_rpm_)
{
    double target_freq_ = 0;
    if (target_rpm_ <= max_rpm && target_rpm_ >= min_rpm)
    {
        //rpm/max_rpm = freq/(microstep*one_round_number_off_step*(max_rpm/60));
        target_freq_ = target_rpm_ * microstep * STEP_PER_REVOLUTION * (1.0 / 60);
    }
    else
    {
        return false;
    }
    if (set_frequency(target_freq_))
    {
        /*PWM_by_HardverTimer = true; //We can use the PWM pin, we don't need to suffer
    warning_bool = false;*/
    }
    else
    {
        /*warning_bool = true;
    PWM_by_HardverTimer = false; //Our suffering shall commence! TODO: implement this other way*/
    }
    return true;
}

bool MotorHandler::set_frequency(double target_freq_)
{ //This is not this simple
    if (target_freq_ == 0)
        return false;
    int overflow_value = round(CPU_FREQ / target_freq_);
    if (overflow_value > 65535)
        return false;
    pwmtimer.pause();
    pwmtimer.setPrescaleFactor(1);
    pwmtimer.setOverflow(overflow_value - 1);
    pwmtimer.setCompare(TIMER_CH1, overflow_value / 2); // 50% duty cycle
    pwmtimer.refresh();
    pwmtimer.resume();
    //delay(1);
    return true;
}

void MotorHandler::updateMotor()
{
    const short TRESHOLD = min_rpm;
    //target_rpm, current_rpm, target_dir, target_is_on
    //if turned_off and we want it to turn on
    if (!current_is_on && target_is_on /*&& current_rpm <= min_rpm*/)
    {
        PWM_turn_on();
        //current_dir = true;
    }
    //if turned on and we want is to turn off
    if (current_rpm <= min_rpm && current_is_on && !target_is_on)
    {
        PWM_turn_off();
        //current_dir = false;
        current_rpm = 0;
    }
    //if it's on
    if (target_is_on)
    {
        if (current_dir == target_dir)
        {
            if (current_rpm < target_rpm)
            {
                current_rpm++;
                //delay(1);
                set_rpm(current_rpm);
            }
            else if (current_rpm > target_rpm)
            {
                current_rpm--;
                //delay(1);
                set_rpm(current_rpm);
            }
        }
        else
        {
            if (current_rpm > min_rpm)
            {
                current_rpm--;
                //delay(1);
                set_rpm(current_rpm);
            }
            else
            {
                current_dir = target_dir;
                change_direction();
            }
        }
    }
    else if (current_is_on)
    {
        current_rpm--;
        set_rpm(current_rpm);
        //delay(1);
    }
    //delay(1);
}

void MotorHandler::updateValues(short target_rpmm, short target_dirr, bool target_is_onn, short max_rpmm, short min_rpmm, short microstepp)
{
    target_rpm = target_rpmm;
    target_dir = target_dirr;
    target_is_on = target_is_onn;
    max_rpm = max_rpmm;
    min_rpm = min_rpmm;
    microstep = microstepp;
}

void MotorHandler::change_direction()
{
    digitalWrite(dirpin, target_dir);
}
