#include "Encoder.h"
#include <stdbool.h>

Encoder::Encoder()
{
  counter = 0;
  upper_bound;
  lower_bound;
  aState;
  aLastState;
  data_changed = false;
  showing_disp = true;
  prev_t = 0;
  prev_read_time = 0;
  last_time_display_was_shown = 0;
  pinMode(ENCODER_INPUT_A, INPUT_PULLUP);
  pinMode(ENCODER_INPUT_B, INPUT_PULLUP);
  aLastState = digitalRead(ENCODER_INPUT_A);
}

int Encoder::read()
{
  int ret = NOTHING_ENCODER;
  aState = digitalRead(ENCODER_INPUT_A);
  long current_millis = millis();
  if (current_millis - prev_read_time > 20) //in case something went wrong and it's not too frequent
  {
    reset_after_display();
  }
  if (aState != aLastState)
  {
    if (digitalRead(ENCODER_INPUT_B) != aState)
    {
      counter++;
      ret = UP_ENCODER;
    }
    else
    {
      counter--;
      ret = DOWN_ENCODER;
    }
    data_changed = true;
    prev_t = current_millis;
  }
  if (data_changed && current_millis - prev_t > 200)
  {
    showing_disp = true;
  }
  aLastState = aState;
  prev_read_time = current_millis;
  return ret;
}

bool Encoder::request_displaying()
{
  return showing_disp; // || ((millis() - last_time_display_was_shown > 500 && data_changed));
}

void Encoder::reset_before_display()
{
  last_time_display_was_shown = millis();
}

void Encoder::reset_after_display()
{
  aLastState = digitalRead(ENCODER_INPUT_A);
  showing_disp = false;
  data_changed = false;
}

int Encoder::handle_rotation(int increment)
{
  counter += increment;
}
