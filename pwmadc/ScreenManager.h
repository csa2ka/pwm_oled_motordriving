#ifndef SCREENMANAGER_H_INCLUDED
#define SCREENMANAGER_H_INCLUDED
#include "Enums.h"
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, PA5, PA7, U8X8_PIN_NONE);
//U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, PB6, PB7, U8X8_PIN_NONE);
//U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE); //use PB6, and PB7

class ScreenManager
{
public:
    Screen current_screen;
    Screen current_setting_screen;
    int current_display_number;
    int start_x;
    int start_y;
    int delta;
    bool pwm_is_on;
    int pwm_voltage;
    int RPM_TEXT_X;
    int pwm_plot[128];// TODO a better way?

public:
    ScreenManager();
    void ShowCurrentScreen();
    void DrawNumberScreen(char *heading, int addition, int number, bool is_main_screen);
    void DrawSettingsScreen(char *heading);
    void circle_moving(int x, int y, int r);
    double projection(double target_lower, double target_upper, double starter_lower, double starter_upper, double starter_value);

    void update(Screen current_screen_,
                int current_display_number_, 
                bool pwm_is_on_, 
                int pwm_voltage_, 
                int pwm_plot_[]);
    void drawPlot();
};

#endif
