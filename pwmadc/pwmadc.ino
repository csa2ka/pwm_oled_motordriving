#include "StateMachine.h"
#include <Arduino.h>
#include <U8g2lib.h>
#include "Encoder.h"
#include "ScreenManager.h"
#include "Button.h"
#include "PWMHandler.h"
#include <EEPROM.h>

#define BUTTON1_PIN PB6 // PB4
#define BUTTON2_PIN PB9
#define BUTTON3_PIN PB7 // PB5
#define BUTTON4_PIN PB8
#define PWM_PIN PA6
#define DIRECTION_PIN PB1
#define ENCODER_B_PIN PB13
#define ENCODER_A_PIN PB14
#define ENCODER_BUTTON_PIN PB12
#define ADC_PIN PA4

const int INPUT_AND_PWM_HANDLING_CALL_TIME = 2000;		//(microseconds) 
volatile bool updateSignalAllowed = true;
bool button_changed_something = false;

Encoder the_encoder;
ScreenManager sc_manager;
Button the_encoder_button(ENCODER_BUTTON_PIN, BUTTON_ENCODER);
Button button1(BUTTON1_PIN, BUTTON1);
StateMachine *stateMachine = new StateMachine(new TimerState);
PWMHandler pwmHandler(PWM_PIN);
HardwareTimer timer(2); // maybe a timerHandler? it could elmininate code from here

void handle_encoder_input(int encoder_input)
{
	switch (encoder_input)
	{
	case NOTHING_ENCODER:
		break;
	case UP_ENCODER:
		stateMachine->EncoderUp();
		break;
	case DOWN_ENCODER:
		stateMachine->EncoderDown();
		break;
	}
}

void handle_button_input(ButtonState button_input, ButtonEnum btnenum)
{
	switch (button_input)
	{
	case PRESSED_AND_RELEASED:
		stateMachine->ButtonPress(btnenum, true);
		button_changed_something = true;
		break;
	case HOLDING_STARTS:
		stateMachine->ButtonPress(btnenum, false);
		button_changed_something = true;
		break;
	}
}

void read_input_and_update_pwmHandler()
{
	handle_encoder_input(the_encoder.read());
	handle_button_input(the_encoder_button.readState(), the_encoder_button.btnenum);
	handle_button_input(button1.readState(), button1.btnenum);
	if (updateSignalAllowed) pwmHandler.updateSignal();
  //stateMachine->Update();
}


/**
 * @brief This function sets up a timer for use in reading input values 
 * and updating the PWM output. period_length is in microseconds.
 * 
 * @param period_length 
 */
void setup_timer(int period_length)
{
	timer.pause();
	timer.setPeriod(period_length);
	timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
	timer.setCompare(TIMER_CH1, 1);
	timer.attachCompare1Interrupt(read_input_and_update_pwmHandler);
	timer.refresh();
	timer.resume();
}

void get_data_from_EEPROM()
{
	stateMachine->setFreq(36000);
	stateMachine->number_to_display = stateMachine->freq;
}

void save_current_rpm_to_eeprom()
{
	stateMachine->EEPROMSave(3, stateMachine->freq);
}

void setup()
{
	get_data_from_EEPROM();
	u8g2.begin();
	setup_timer(INPUT_AND_PWM_HANDLING_CALL_TIME);	
	pinMode(LED_BUILTIN, OUTPUT);
}

/**
 * Reads the analog value of a pin and returns the average of multiple readings.
 * 
 * This function takes multiple readings of the analog value of the specified pin, and returns the average of these readings. This can help to reduce noise in the readings and improve the accuracy of the returned value.
 * 
 * @param pin The analog pin to read.
 * @param numReadings The number of readings to take and average.
 * @return The average of the readings.
 */
int readADC(int pin, int numReadings)
{
	int sum = 0;
	for (int i = 0; i < numReadings; i++)
	{
		sum += analogRead(pin);
		// delay(1);  // delay to allow the signal to stabilize
	}
	return sum / numReadings;
}

int maxVoltageFreq = 0;
long last_adc_max_search_time = millis();

/**
 * Finds the frequency with the highest voltage using a brute force search.
 * 
 * This function sets the PWM frequency to a range of values between 37000 
 * and 40000 Hz, and measures the voltage at each frequency. The frequency
 *  with the highest voltage is stored in the global variable maxVoltageFreq. 
 * The voltage measurements are also stored in the pwm_plot array of the stateMachine object.
 *
 * @warning This function sets the updateSignalAllowed flag to false while 
 * it is running, to prevent updates to the signal from being sent to the PWM device.
 */
void findMaxFreq()
{
	int maxVoltage = 0;
	if (stateMachine->target_pwm_is_on && millis() - last_adc_max_search_time > 10000)
	{
		updateSignalAllowed = false;
		for (int i = 0; i < 128; i++)
		{
			int actFr = 37000 + (127 - i) * ((40000 - 37000) / 128);
			pwmHandler.set_frequency(actFr);
			delay(5);
			int readVoltage = readADC(ADC_PIN, 20);
			if (readVoltage > maxVoltage)
			{
				maxVoltage = readVoltage;
				maxVoltageFreq = actFr;
			}
			stateMachine->pwm_plot[i] = readVoltage;
		}
		last_adc_max_search_time = millis();
		updateSignalAllowed = true;
	}
}

int led_value = HIGH;
void debugLedBlink(){
	digitalWrite(LED_BUILTIN, led_value);
	if (led_value == HIGH)
	{
		led_value = LOW;
	}
	else
	{
		led_value = HIGH;
	}
}

unsigned long lastLoopStartTime =0;
void loop() // loop runs as it pleases: it determined by the length of displaying the screen, it's around 20-25 FPS at this time
{
  if (millis() - lastLoopStartTime >500){
  	lastLoopStartTime = millis();
  	findMaxFreq();
  	debugLedBlink();
  	pwmHandler.updateValues(stateMachine->freq, stateMachine->target_pwm_is_on);
  	//save_current_rpm_to_eeprom();
    stateMachine->Update();
  	sc_manager.update(stateMachine->current_screen,
  					  stateMachine->number_to_display,
  					  stateMachine->target_pwm_is_on,
  					  stateMachine->pwm_voltage,
  					  stateMachine->pwm_plot);
  	sc_manager.ShowCurrentScreen();
  }
}
