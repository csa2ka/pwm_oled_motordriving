#include "ScreenManager.h"
#include <stdbool.h>
#include "Enums.h"

ScreenManager::ScreenManager()
{
    current_screen = RPM_SCREEN;
    current_setting_screen = MIN_SETTING_SCREEN;
    current_display_number = 0;
    start_x = 16;
    start_y = 62;
    delta = 32;
    RPM_TEXT_X = 77;
    pwm_is_on = false;
    pwm_voltage = 0;
    for (int i = 0; i < 128; i++) {
        pwm_plot[i] = 20;
    }
}

void ScreenManager::ShowCurrentScreen()
{
    DrawNumberScreen("  ", RPM_TEXT_X - start_x, current_display_number, true);
    u8g2.sendBuffer();
}

void ScreenManager::circle_moving(int x, int y, int r)
{
    int period_time = 500; //10-50   one_round_s_time = 1 / round_per_minute
    int segment = ((millis() % period_time) / (period_time / 4)) % 4;
    switch (segment)
    {
    case 0:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_UPPER_RIGHT);
        break;
    case 1:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_UPPER_LEFT);
        break;
    case 2:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_LOWER_LEFT);
        break;
    case 3:
        u8g2.drawCircle(x, y, r, U8G2_DRAW_LOWER_RIGHT);
        break;
    default:
        break;
    }
}

double ScreenManager::projection(double target_lower, double target_upper, double starter_lower, double starter_upper, double starter_value)
{
    //  (x - a) / (b - a) = (X - A) / (B - A)
    //  x - a = (X - A) / (B - A) * (b - a)
    //  x = (X - A) / (B - A) * (b - a) + a
    if (starter_value <= starter_lower) return target_lower;
    if (starter_value >= starter_upper) return target_upper;
    return ((starter_value - starter_lower) / (starter_upper - starter_lower)) * (target_upper - target_lower) + target_lower;
}

void ScreenManager::drawPlot()
{
    for (int i = 0; i < 128; i++) {
        u8g2.drawPixel(i, pwm_plot[i]);
    }
}

void ScreenManager::DrawNumberScreen(char *heading, int addition, int number, bool is_main_screen = false)
{
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_logisoso16_tf); //u8g2_font_freedoomr10_mu  );
    if (is_main_screen)
    {
        if (pwm_is_on)
        {
            circle_moving(6, 40, 6);
        }
        //u8g2.drawStr(start_x, 16,  u8x8_utoa(pwm_voltage));
        u8g2.drawStr(start_x + 50, 16,  u8x8_utoa(number));
        /*double a = (double) 0;
        double b = (double) 500;
        double A = 35000.0;
        double B = 40000.0;
        double nn = ((number-a)/(b-a))*(B-A) + A;
        u8g2.drawStr(start_x, 48,  u8x8_utoa(nn));*/
        double p_y = projection(1,63,1,600,pwm_voltage); // TODO: better way
        double p_x = projection(1,127,37000,40000,number);
        //u8g2.drawStr(start_x, 32,  u8x8_utoa(pwm_plot[50]));
        //u8g2.drawStr(start_x, 48,  u8x8_utoa(pwm_plot[50]));
        //pwm_plot[int(p_x)] = p_y;
        //drawPlot();
    }                                              // write something to the internal memory
    u8g2.drawStr(start_x + addition, 16, heading); // write something to the internal memory

    u8g2.sendBuffer();
}

void ScreenManager::DrawSettingsScreen(char *heading)
{
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_logisoso16_tf);
    u8g2.drawStr(15, 16, heading);
    u8g2.sendBuffer();
}

void ScreenManager::update(Screen current_screen_, 
                           int current_display_number_, 
                           bool motor_is_on_, 
                           int pwm_voltage_,
                           int pwm_plot_[]
                           )
{
    current_screen = current_screen_;
    //current_setting_screen = current_setting_screen_;
    current_display_number = current_display_number_;
    pwm_is_on = motor_is_on_;
    pwm_voltage = pwm_voltage_;
    for (int i = 0; i < 128; i++) {
      int p_y = int(projection(1,63,1,600,int(pwm_plot_[i])));
      int p_x = int(projection(1,127,37000,40000,i));
      pwm_plot[i] = p_y; 
    }
}
