
#ifndef STATEMACHINE_H
#define STATEMACHINE_H
#include "Enums.h"
#include <EEPROM.h>
//#include "MaxRPMSettingScreen.h"

class StateMachine;

class State
{
    /**
   * @var Context
   */
protected:
    StateMachine *context_;

public:
    virtual ~State() {}

    void set_context(StateMachine *context) { this->context_ = context; }
    virtual void HandleEncoderUp() = 0;
    virtual void HandleEncoderDown() = 0;
    virtual void HandleButtonPress(ButtonEnum whichone, bool isshort) = 0;
    virtual void Update() = 0;
};

/**
 * The Context defines the interface of interest to clients. It also maintains a
 * reference to an instance of a State subclass, which represents the current
 * state of the Context.
 */

class StateMachine
{
    /**
   * @var State A reference to the current state of the Context.
   */
private:
    State *state_;

public:
    Screen current_screen;
    int freq;
    int number_to_display;
    bool target_pwm_is_on; 
    int pwm_plot[128];

    int baseTime;       //(sec)
    unsigned long lastStartingTimeMillis;   //(millisec)
    bool isTimerGoing; 

    int pwm_voltage;
    bool sweaping;

    StateMachine(State *state) : state_(nullptr)
    {
        target_pwm_is_on = false;
        number_to_display = freq;
        pwm_voltage = 0;
        current_screen = RPM_SCREEN;
        sweaping = false;
        for (int i = 0; i < 128; i++) {
            pwm_plot[i] = 0;
        }

        this->TransitionTo(state);
    }
    ~StateMachine()
    {
        delete state_;
    }
    void EEPROMSave(int address, int number);
    int EEPROMRead(int address);
    void TransitionTo(State *state);
    void EncoderUp();
    void EncoderDown();
    void ButtonPress(ButtonEnum whichone, bool isshort);
    void setFreq(int frequency);
    void Update();
};

class RPMScreen : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
    //void update() override;
};

class TimerState : public State
{
public:
    void HandleEncoderUp() override;
    void HandleEncoderDown() override;
    void HandleButtonPress(ButtonEnum whichone, bool isshort) override;
    void Update() override;
};

#endif
