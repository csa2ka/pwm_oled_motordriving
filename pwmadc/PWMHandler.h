#ifndef MOTORHANDLING_H
#define MOTORHANDLING_H

HardwareTimer pwmtimer(3);

class PWMHandler
{
private:
public:
    const long CPU_FREQ = 72000000; //Hertz
    int current_freq = 0;
    int target_freq = 0;
    
    const short SWEAPING_DELTA_FREQ = 5;       //(Hz) during sweaping this is the frequency stepsize 
    const int SWEAPING_AMPLITUDE = 500;       //(Hz) how much Hz we want to sweap around
    const int SWEAPING_PERIOD_TIME = 1000;    //(millisecond) this is how long one sweap period will last
    const int SWEAPING_DELTA_TIME = SWEAPING_PERIOD_TIME / ((SWEAPING_AMPLITUDE * 4) / SWEAPING_DELTA_FREQ);      //(millisecond)
    int sweaping_offset_freq = 0;
    signed char sweaping_delta_sign = 1;
    unsigned long last_offset_change_time = 0;
    
    bool target_is_on = false;
    bool current_is_on = false;

    short pwm_pin;
    PWMHandler(short steppinn);
    void PWM_turn_off();
    void PWM_turn_on();
    bool set_frequency();
    void updateOffsetFrequency();
    bool set_frequency(double frequency);
    void updateValues(int target_freq_, bool target_is_on_);
    void updateSignal();
    int findMaxVoltageFrequency(int minFr, int endFr, int stepsize, int pin);
};

#endif
