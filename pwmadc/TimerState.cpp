#include "StateMachine.h"
#include "Enums.h"
#include <EEPROM.h>

void TimerState::HandleEncoderUp()
{
    // increase runtime
    //TODO: do this better
    this->context_->baseTime += 1; 
    if (this->context_->baseTime > 500) this->context_->baseTime = 0; 
}

void TimerState::HandleEncoderDown()
{
    // decrease runtime
    //TODO: do this better
    this->context_->baseTime -= 1; 
    if (this->context_->baseTime < 0) this->context_->baseTime = 0; 
}

void TimerState::HandleButtonPress(ButtonEnum whichone, bool isshort)
{
    switch (whichone)
    {
    case BUTTON1:
        if (isshort)
        {
            //start / stop running
            if (this->context_->isTimerGoing){
                this->context_->isTimerGoing = false;
                this->context_->target_pwm_is_on = false;
            } else {
                this->context_->isTimerGoing = true;
                this->context_->lastStartingTimeMillis = millis();
                this->context_->target_pwm_is_on = true;
            }
        }
        break;
    case BUTTON_ENCODER:
        if (!isshort)
        {
            //TODO: change mode
            /*this->context_->number = this->context_->current_rpm;
            this->context_->current_screen = RPM_SCREEN;
            this->context_->TransitionTo(new RPMScreen);*/
        }
        break;
    default:
        break;
    }
}

void TimerState::Update()
{
  if (this->context_->isTimerGoing){
    int what_to_display =  this->context_->baseTime - (millis() - this->context_->lastStartingTimeMillis) / 1000; 
    if (what_to_display <= 0) {
      what_to_display = this->context_->baseTime;
      this->context_->isTimerGoing = false;
      this->context_->target_pwm_is_on = false;
    }
    this->context_->number_to_display = what_to_display;
  } else {
    this->context_->number_to_display = this->context_->baseTime;
  }
}
