#include "PWMHandler.h"

PWMHandler::PWMHandler(short steppinn)
{
    pwm_pin = steppinn;
}

void PWMHandler::PWM_turn_off()
{
    pinMode(pwm_pin, OUTPUT);
    current_is_on = false;
}

void PWMHandler::PWM_turn_on()
{
    pinMode(pwm_pin, PWM);
    current_is_on = true;
}

bool PWMHandler::set_frequency(double target_freq_)
{   //This is not this simple
    if (target_freq_ == 0)
        return false;
    int overflow_value = round(CPU_FREQ / target_freq_);
    if (overflow_value > 65535)
        return false;
    pwmtimer.pause();
    pwmtimer.setPrescaleFactor(1);
    pwmtimer.setOverflow(overflow_value - 1);
    pwmtimer.setCompare(TIMER_CH1, overflow_value / 2); // 50% duty cycle
    pwmtimer.refresh();
    pwmtimer.resume();
    //delay(1);
    current_freq = target_freq_;
    return true;
}


void PWMHandler::updateOffsetFrequency(){        //maybe put in utils, do we have utils?
    // if the last time we changed the offset frequency was longer than sweaping delta time
    unsigned long current_millis = millis();
    if (current_millis - last_offset_change_time > SWEAPING_DELTA_TIME)
    {
        //change offsetFreq by delta, the sign of this delta is considered too
        sweaping_offset_freq += SWEAPING_DELTA_FREQ * sweaping_delta_sign;
        //change direction when amplitude is reached
        if (sweaping_offset_freq >= SWEAPING_AMPLITUDE) sweaping_delta_sign = -1;
        if (sweaping_offset_freq <= -SWEAPING_AMPLITUDE) sweaping_delta_sign = 1; 
        last_offset_change_time = current_millis;
    }
}


void PWMHandler::updateSignal()
{
    //if turned_off and we want it to turn on
    if (!current_is_on && target_is_on)
    {
        PWM_turn_on();
    }
    //if turned on and we want is to turn off
    if (current_is_on && !target_is_on)
    {
        PWM_turn_off();
    }

    
    updateOffsetFrequency();
    int target_and_sweaping_freq = target_freq + sweaping_offset_freq;
    if (target_and_sweaping_freq != current_freq){
        set_frequency(target_and_sweaping_freq);
    } 
}

void PWMHandler::updateValues(int target_freq_, bool target_is_on_)
{
    target_freq = target_freq_;
    target_is_on = target_is_on_;
}
